#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 22:17:52 2021

@author: usrsiac
"""
import json
from py2neo import Graph
graph = Graph("bolt://localhost:7687", auth=("neo4j", "123456789"))


#Cargamos los esquemas de medicina y vivo
consulta=graph.run('match(i:instancia)-[x:rdfs__subClassOf]->(a:title) return i,x,a').to_data_frame();

js = consulta.to_json(orient = 'index')
print(js)