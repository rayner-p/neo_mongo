#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 17:10:43 2021

@author: usrsiac
"""
from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello():
    return 'Web Service Prueba'
