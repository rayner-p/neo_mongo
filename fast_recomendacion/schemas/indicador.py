def indicadorEntity(item) -> dict:
    return {
        'id': str(item["_id"]),
        'pagina': item['pagina'],
        'titulo': item['titulo'],
        'indicadores': item['indicadores'],
        'EncoderI': item['EncoderI'],
        'EncoderT': item['EncoderT']
    }
def indicadoresEntity(entity) -> list:
    return [indicadorEntity(item) for item in entity]