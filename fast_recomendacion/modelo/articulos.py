from pydantic import BaseModel


class Articulos(BaseModel):
    id: str
    link: str
    title: str
    abstract: str
    authors: str
    fuente: str
    key: str
