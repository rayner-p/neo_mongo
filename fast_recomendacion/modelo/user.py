from time import time
from typing import List, Optional
from pydantic import BaseModel
from datetime import date, datetime, time


class User(BaseModel):
    uid: str
    password: str
    username: str
    email: str
    displayName: str
    photoURL: str
    emailVerified: bool
