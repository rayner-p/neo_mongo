import time
import jwt  # PyJWT
from decouple import config
from config.db import conn

JWT_SECRET = config('secret')
JWT_ALGORITHM = config('algorithm')


def token_response(token: str, usr: str):
    us = conn.actividades.user.find_one({'username': usr})
    us['_id'] = str(us['_id'])
    return {
        'resultado': us,
        'jwt': token
    }


def signJWT(userId: str):
    payload = {
        'userId': userId,
        'expiry': time.time() + 86400
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return token_response(token, userId)


def decodeJWT(token: str):
    try:
        decode_token = jwt.decode(token, JWT_SECRET, algorithms=JWT_ALGORITHM)
        return decode_token if decode_token['expires'] >= time.time() else None
    except:
        return {}
