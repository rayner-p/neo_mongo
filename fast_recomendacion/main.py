#from urllib import request
from fastapi import FastAPI
from ruta.actividad import actividad
from ruta.objetoActivity import objetoActivity
from starlette.middleware.cors import CORSMiddleware as CORSMiddleware  # noqa
from functions_jwt import signJWT
from config.db import conn
from models.login import Login
from routes.recurso import rcso
from routes.habilidad import hbld
from routes.indicador import idcd
from routes.area import area
from routes.icd10 import icd10
from routes.bloque import bloque
from routes.competencia import competencia
from routes.new_resources import newrecurso
from werkzeug.security import check_password_hash
import asyncio
from routes.user import user


app = FastAPI()

app.include_router(actividad)
app.include_router(objetoActivity)
app.include_router(user)
app.include_router(actividad)
app.include_router(rcso)
app.include_router(hbld)
app.include_router(idcd)
app.include_router(area)
app.include_router(icd10)
app.include_router(bloque)
app.include_router(competencia)
app.include_router(newrecurso)

origins = [
    "*",
    "http://localhost:4200",
    "http://localhost:8000",
    "http://localhost:4201",
    "http://localhost:51235",
    "http://localhost:80",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {
        "Documentación": {
            "Swagger Open-API": "/docs",
            "ReDOC": "/redoc"
        },
    }


@app.post("/login")
async def user_login(usr: Login):
    c = 0
    l_usr = list(conn.actividades.user.find())
    for i in l_usr:
        if i['username'] == usr.username and check_password_hash(i['password'], usr.password):
            if(i['rol'] == 'docente' or i['rol'] == 'admin'):
                # await send_email_async('Hola {} has iniciado con exito la sesion en el repositorio.'.format(i['username']), i['username'])
                return signJWT(i['username'])
            else:
                return signJWT(i['username'])
        else:
            c += 1
            if c == len(l_usr):
                return {"message": "Usuario o contraseña incorrectos"}
                # return {}
