from time import time
from typing import List, Optional
from pydantic import BaseModel
from datetime import date, datetime, time


class User(BaseModel):
    id: Optional[str]
    cedula: str
    nombre: str
    apellido: str
    fechaNacimiento: str
    unidadEducativa: str
    username: Optional[str]
    password: Optional[str]
    capacidad_especial: Optional[bool]  
    diagnostico_medico: Optional[List]
    diagnostico_lenguaje: Optional[List]