from typing import Optional
from pydantic import BaseModel

class Habilidad(BaseModel):
    id: str
    Area: str
    Bloque: str
    Competencia: str
    cod: int
    Habilidad: str