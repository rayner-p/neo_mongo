from typing import Optional
from pydantic import BaseModel

class Actividad(BaseModel):
    id: str
    pagina: int
    N_Actividad: int
    titulo: str
    actividades: str
    analisis: str
    indicadores: str