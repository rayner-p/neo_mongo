from pydantic import BaseModel

class Actividad(BaseModel):
    id: str
    pagina: int
    titulo: str
    indicadores: str
    EncoderI: int
    EncoderT: int

