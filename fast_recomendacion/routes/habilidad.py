# Este archivo usa el encoding: utf-8
from fastapi import APIRouter
from config.db import conn
from py2neo import Graph
from bson.objectid import ObjectId
import time
import pandas as pd

hbld = APIRouter()


@hbld.get('/habilidad/{id}')
async def find_one_habi(id: str):
    # print(id)
    start = time.time()
    d_hab = pd.DataFrame(list(conn.actividades.NewActividades.find()))
    get_one_hab = d_hab.loc[d_hab['_id'] == ObjectId(id)]
    # print(get_one_hab)
    if(len(get_one_hab) > 0):
        get_one_hab = get_one_hab.to_dict('records')
        get_one_hab[0]['_id'] = str(get_one_hab[0]['_id'])
        get_one_hab[0]['Area'] = str(get_one_hab[0]['_id'])
        get_one_hab[0]['Bloque'] = str(get_one_hab[0]['_id'])
        get_one_hab[0]['Competencia'] = str(get_one_hab[0]['Competencia'])
        ha = get_one_hab[0]
        end = time.time()
        print(end - start)
        return ha
    else:
        return


@hbld.get('/habilidades')
async def find_all_habilidades():
    start = time.time()
    l_h = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    q_id = "match(n:Actividad) where exists((n)<-[:APPS]-()) OR exists((n)<-[:CUENTOS]-()) OR exists((n)<-[:JUEGOS]-()) OR exists((n)<-[:LIBROS]-()) OR exists((n)<-[:TWEETS]-()) OR exists((n)<-[:VIDEOS]-()) return n.texto as texto"

    resu_query = gds.run(q_id).to_data_frame()

    d_hab = pd.DataFrame(list(conn.actividades.NewActividades.find()))
    for i in resu_query.iloc[:, 0]:
        get_hab = d_hab.loc[d_hab['_id'] == ObjectId(i)]
        if(len(get_hab) > 0):
            get_hab = get_hab.to_dict('records')
            get_hab[0]['_id'] = str(get_hab[0]['_id'])
            get_hab[0]['Area'] = str(get_hab[0]['Area'])
            get_hab[0]['Bloque'] = str(get_hab[0]['Bloque'])
            get_hab[0]['Competencia'] = str(get_hab[0]['Competencia'])
            l_h.append(get_hab[0])
    end = time.time()
    print(end - start)
    return l_h


@hbld.post("/habilidades")
async def create_new_actividad(new_activities: list):
    start = time.time()
    nuevas_actividades = new_activities
    # print(nuevas_actividades)
    l_act_gua = []
    for nueva_actividad in nuevas_actividades:
        # print(nueva_actividad)
        #print(unicodedata.normalize('NFKD', nueva_actividad['Area']).encode('ASCII', 'ignore'))
        # print(nueva_actividad)
        if(nueva_actividad['capa_espe'] == False):
            try:
                if(nueva_actividad['indicadores'] != ''):
                    try:
                        nueva_actividad['indicadores'] = ObjectId(
                            nueva_actividad['indicadores'])
                    except:
                        ind = {"indicadores": nueva_actividad['indicadores']}
                        id_i = conn.actividades.Indicadores.insert_one(
                            ind).inserted_id
                        nueva_actividad['indicadores'] = ObjectId(id_i)
                    id_a = conn.actividades.ActividadesN.insert_one(
                        nueva_actividad).inserted_id
                    # print(id_a)
                    l_act_gua.append(str(id_a))
                    end = time.time()
                    print(end - start)
                    # return {"message": "Nueva Habilidad Guardada", "id": str(id_a)}
            except:
                if(len(nueva_actividad['Area']) == 24 and len(nueva_actividad['Bloque']) == 24 and len(nueva_actividad['Competencia']) == 24):
                    # print("if")
                    # print(nueva_actividad)
                    ar = conn.actividades.Area.find_one(
                        {'_id': ObjectId(nueva_actividad['Area'])})
                    if(ar != None):
                        nueva_actividad['Area'] = ar['_id']
                    ###nueva_actividad['Area'] = ar['_id']
                    bl = conn.actividades.Bloque.find_one(
                        {'_id': ObjectId(nueva_actividad['Bloque'])})
                    print(bl)
                    if(bl != None):
                        nueva_actividad['Bloque'] = bl['_id']
                    ###nueva_actividad['Bloque'] = bl['_id']
                    co = conn.actividades.Competencia.find_one(
                        {'_id': ObjectId(nueva_actividad['Competencia'])})
                    print(co)
                    if(co != None):
                        nueva_actividad['Competencia'] = co['_id']
                    ###nueva_actividad['Competencia'] = co['_id']

                    id_a = conn.actividades.ActividadesN.insert_one(
                        nueva_actividad).inserted_id
                    l_act_gua.append(str(id_a))
                    end = time.time()
                    print(end - start)
                    # return {"message": "Nueva Habilidad Guardada", "id": str(id_a)}
                # print(nueva_actividad)
                else:
                    # print("else")
                    # print(nueva_actividad)
                    ar1 = conn.actividades.Area.find_one(
                        {'titulo': nueva_actividad['Area']})
                    # print(ar1)
                    if(ar1 != None):
                        nueva_actividad['Area'] = ar1['_id']
                    bl2 = conn.actividades.Bloque.find_one(
                        {'titulo': nueva_actividad['Bloque']})
                    # print(bl2)
                    if(bl2 != None):
                        nueva_actividad['Bloque'] = bl2['_id']
                    co2 = conn.actividades.Competencia.find_one(
                        {'titulo': nueva_actividad['Competencia']})
                    # print(co2)
                    if(co2 != None):
                        nueva_actividad['Competencia'] = co2['_id']
                    id_a = conn.actividades.ActividadesN.insert_one(
                        nueva_actividad).inserted_id
                    # print(id_a)
                    l_act_gua.append(str(id_a))
                    end = time.time()
                    print(end - start)
                    # return {"message": "Nueva Habilidad Guardada", "id": str(id_a)}

        else:
            try:
                if(nueva_actividad['indicadores'] != ''):
                    try:
                        nueva_actividad['indicadores'] = ObjectId(
                            nueva_actividad['indicadores'])
                    except:
                        ind = {"indicadores": nueva_actividad['indicadores']}
                        id_i = conn.actividades.Indicadores.insert_one(
                            ind).inserted_id
                        nueva_actividad['indicadores'] = ObjectId(id_i)
                    id_a = conn.actividades.ActividadesND.insert_one(
                        nueva_actividad).inserted_id
                    # print(id_a)
                    l_act_gua.append(str(id_a))
                    end = time.time()
                    print(end - start)
                    # return {"message": "Nueva Habilidad Guardada", "id": str(id_a)}
            except:
                if(len(nueva_actividad['Area']) == 24 and len(nueva_actividad['Bloque']) == 24 and len(nueva_actividad['Competencia']) == 24):
                    # print("if")
                    # print(nueva_actividad)
                    ar = conn.actividades.Area.find_one(
                        {'_id': ObjectId(nueva_actividad['Area'])})
                    if(ar != None):
                        nueva_actividad['Area'] = ar['_id']
                    ###nueva_actividad['Area'] = ar['_id']
                    bl = conn.actividades.Bloque.find_one(
                        {'_id': ObjectId(nueva_actividad['Bloque'])})
                    # print(bl)
                    if(bl != None):
                        nueva_actividad['Bloque'] = bl['_id']
                    ###nueva_actividad['Bloque'] = bl['_id']
                    co = conn.actividades.Competencia.find_one(
                        {'_id': ObjectId(nueva_actividad['Competencia'])})
                    # print(co)
                    if(co != None):
                        nueva_actividad['Competencia'] = co['_id']
                    ###nueva_actividad['Competencia'] = co['_id']

                    id_a = conn.actividades.ActividadesND.insert_one(
                        nueva_actividad).inserted_id
                    l_act_gua.append(str(id_a))
                    end = time.time()
                    print(end - start)
                    # return {"message": "Nueva Habilidad Guardada", "id": str(id_a)}
                # print(nueva_actividad)
                else:
                    # print("else")
                    # print(nueva_actividad)
                    ar1 = conn.actividades.Area.find_one(
                        {'titulo': nueva_actividad['Area']})
                    # print(ar1)
                    if(ar1 != None):
                        nueva_actividad['Area'] = ar1['_id']
                    bl2 = conn.actividades.Bloque.find_one(
                        {'titulo': nueva_actividad['Bloque']})
                    # print(bl2)
                    if(bl2 != None):
                        nueva_actividad['Bloque'] = bl2['_id']
                    co2 = conn.actividades.Competencia.find_one(
                        {'titulo': nueva_actividad['Competencia']})
                    # print(co2)
                    if(co2 != None):
                        nueva_actividad['Competencia'] = co2['_id']
                    id_a = conn.actividades.ActividadesND.insert_one(
                        nueva_actividad).inserted_id
                    # print(id_a)
                    l_act_gua.append(str(id_a))
                    end = time.time()
                    print(end - start)
                    # return {"message": "Nueva Habilidad Guardada", "id": str(id_a)}
    # print(l_act_gua)
    return l_act_gua
