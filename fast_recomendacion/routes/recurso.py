from bson.objectid import ObjectId
from fastapi import APIRouter
from config.db import conn
from py2neo import Graph
import time
import pandas as pd

rcso = APIRouter()


def isNaN(num):
    return num != num


@rcso.get('/recurso/{id}')
async def find_one_rcso(id: str):
    print('id_recurso', id)
    start = time.time()
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    q_id = ("match (n:Actividad)<-[:APPS]-(r:RecursoApp) WHERE n.texto ='" +
            id+"' return r.recurso as recursoapp")
    q_id2 = ("match (n:Actividad)<-[:CUENTOS]-(r:RecursoCuento) WHERE n.texto = '" +
             id+"' return r.recurso as recursocuento")
    q_id3 = ("match (n:Actividad)<-[:JUEGOS]-(r:RecursoJuego) WHERE n.texto = '" +
             id+"' return r.recurso as recursojuego")
    q_id4 = ("match (n:Actividad)<-[:LIBROS]-(r:RecursoLibro) WHERE n.texto = '" +
             id+"' return r.recurso as recursolibro")
    q_id5 = ("match (n:Actividad)<-[:TWEETS]-(r:RecursoTweet) WHERE n.texto = '" +
             id+"' return r.recurso as recursotweet")
    q_id6 = ("match (n:Actividad)<-[:VIDEOS]-(r:RecursoVideo) WHERE n.texto = '" +
             id+"' return r.recurso as recursovideo")
    resu_query = gds.run(q_id).to_data_frame()
    resu_query2 = gds.run(q_id2).to_data_frame()
    resu_query3 = gds.run(q_id3).to_data_frame()
    resu_query4 = gds.run(q_id4).to_data_frame()
    resu_query5 = gds.run(q_id5).to_data_frame()
    resu_query6 = gds.run(q_id6).to_data_frame()
    """CODIGO PARA LA RECOMENDACION"""
    l_nod_sim = []
    nod_sim = "MATCH (a:Actividad) WHERE a.texto = '"+id +"' RETURN a.texto AS texto, (a)<-[:SIMILARIDAD]-() AS nodos_simi, size((a)<-[:SIMILARIDAD]-()) AS tamaño"
    res_nod_sim = gds.run(nod_sim).to_data_frame()
    # print(res_nod_sim)
    d_act = pd.DataFrame(list(conn.actividades.libro.find()))
    d_hab = pd.DataFrame(list(conn.actividades.NewActividades.find()))
    d_act_dis = pd.DataFrame(
        list(conn.actividades.ActividadesCapacidades.find()))
    d_nue_act = pd.DataFrame(list(conn.actividades.ActividadesN.find()))
    d_nue_act_dis = pd.DataFrame(list(conn.actividades.ActividadesND.find()))
    # print(d_act)
    # print(d_hab)
    for i in res_nod_sim.index:
        # print(res_nod_sim['nodos_simi'][i])
        for j in res_nod_sim['nodos_simi'][i]:
            # print(j.end_node.values())
            get_act = d_act.loc[d_act['_id'] ==
                                ObjectId(list(j.end_node.values())[0])]
            get_hab = d_hab.loc[d_hab['_id'] ==
                                ObjectId(list(j.end_node.values())[0])]
            get_act_dis = d_act_dis.loc[d_act_dis['_id']
                                        == ObjectId(list(j.end_node.values())[0])]
            get_nue_act = d_nue_act.loc[d_nue_act['_id']
                                        == ObjectId(list(j.end_node.values())[0])]
            get_nue_act_dis = d_nue_act_dis.loc[d_nue_act_dis['_id'] == ObjectId(
                list(j.end_node.values())[0])]
            # print(get_act)
            if(len(get_act) > 0):
                get_act = get_act.to_dict('records')
                get_act[0]['_id'] = str(get_act[0]['_id'])
                l_nod_sim.append(get_act[0])
            elif(len(get_hab) > 0):
                get_hab = get_hab.to_dict('records')
                get_hab[0]['_id'] = str(get_hab[0]['_id'])
                get_hab[0]['Area'] = str(get_hab[0]['Area'])
                get_hab[0]['Bloque'] = str(get_hab[0]['Bloque'])
                get_hab[0]['Competencia'] = str(get_hab[0]['Competencia'])
                l_nod_sim.append(get_hab[0])
            elif(len(get_act_dis) > 0):
                get_act_dis = get_act_dis.to_dict('records')
                get_act_dis[0]['_id'] = str(get_act_dis[0]['_id'])
                try:
                    if(isNaN(get_act_dis[0]['titulo'])):
                        get_act_dis[0]['titulo'] = ""
                except:
                    pass
                # print(get_act_dis)
                l_nod_sim.append(get_act_dis[0])
            elif(len(get_nue_act) > 0):
                get_nue_act = get_nue_act.to_dict('records')
                get_nue_act[0]['_id'] = str(get_nue_act[0]['_id'])
                if(isNaN(get_nue_act[0]['indicadores']) == False):
                    get_nue_act[0]['indicadores'] = str(
                        get_nue_act[0]['indicadores'])
                    try:
                        if(isNaN(get_nue_act[0]['Area'])):
                            get_nue_act[0]['Area'] = ""
                        if(isNaN(get_nue_act[0]['Bloque'])):
                            get_nue_act[0]['Bloque'] = ""
                        if(isNaN(get_nue_act[0]['Competencia'])):
                            get_nue_act[0]['Competencia'] = ""
                        if(isNaN(get_nue_act[0]['codigo'])):
                            get_nue_act[0]['codigo'] = ""
                    except:
                        pass
                    l_nod_sim.append(get_nue_act[0])
                else:
                    get_nue_act[0]['Area'] = str(get_nue_act[0]['Area'])
                    get_nue_act[0]['Bloque'] = str(get_nue_act[0]['Bloque'])
                    get_nue_act[0]['Competencia'] = str(
                        get_nue_act[0]['Competencia'])
                    try:
                        if(isNaN(get_nue_act[0]['indicadores'])):
                            get_nue_act[0]['indicadores'] = ""
                        if(isNaN(get_nue_act[0]['codigo'])):
                            get_nue_act[0]['codigo'] = ""
                    except:
                        pass
                    l_nod_sim.append(get_nue_act[0])
            elif(len(get_nue_act_dis) > 0):
                get_nue_act_dis = get_nue_act_dis.to_dict('records')
                get_nue_act_dis[0]['_id'] = str(get_nue_act_dis[0]['_id'])
                if(isNaN(get_nue_act_dis[0]['indicadores']) == False):
                    get_nue_act_dis[0]['indicadores'] = str(
                        get_nue_act_dis[0]['indicadores'])
                    try:
                        if(isNaN(get_nue_act_dis[0]['Area'])):
                            get_nue_act_dis[0]['Area'] = ""
                        if(isNaN(get_nue_act_dis[0]['Bloque'])):
                            get_nue_act_dis[0]['Bloque'] = ""
                        if(isNaN(get_nue_act_dis[0]['Competencia'])):
                            get_nue_act_dis[0]['Competencia'] = ""
                        if(isNaN(get_nue_act_dis[0]['codigo'])):
                            get_nue_act_dis[0]['codigo'] = ""
                    except:
                        pass
                    l_nod_sim.append(get_nue_act_dis[0])
                else:
                    get_nue_act_dis[0]['Area'] = str(
                        get_nue_act_dis[0]['Area'])
                    get_nue_act_dis[0]['Bloque'] = str(
                        get_nue_act_dis[0]['Bloque'])
                    get_nue_act_dis[0]['Competencia'] = str(
                        get_nue_act_dis[0]['Competencia'])
                    try:
                        if(isNaN(get_nue_act_dis[0]['indicadores'])):
                            get_nue_act_dis[0]['indicadores'] = ""
                        if(isNaN(get_nue_act_dis[0]['codigo'])):
                            get_nue_act_dis[0]['codigo'] = ""
                    except:
                        pass
                    l_nod_sim.append(get_nue_act_dis[0])

            '''elif(len(get_nue_act) > 0):
                get_nue_act = get_nue_act.to_dict('records')
                print(get_nue_act)
            else:
                get_nue_act_dis = get_nue_act_dis.to_dict('records')
                print(get_nue_act_dis)'''
    """FIN CODIGO PARA LA RECOMENDACION"""

    # print(resu_query)
    # print(resu_query2)
    # print(resu_query3)
    # print(resu_query4)
    # print(resu_query5)
    # print(resu_query6)
    l_g = []
    l_a = []
    l_c = []
    l_j = []
    l_l = []
    l_t = []
    l_v = []
    if len(resu_query) > 0:
        d_app = pd.DataFrame(list(conn.actividades.RecursosAppsR.find()))
        for i in resu_query.index:
            get_app = d_app.loc[d_app['_id'] ==
                                ObjectId(resu_query["recursoapp"][i])]
            get_app = get_app.to_dict('records')
            get_app[0]['_id'] = str(get_app[0]['_id'])
            a = get_app[0]
            l_a.append(a)
    if len(resu_query2) > 0:
        d_cue = pd.DataFrame(list(conn.actividades.RecursoCuentos.find()))
        for i in resu_query2.index:
            get_cue = d_cue.loc[d_cue['_id'] == ObjectId(
                resu_query2["recursocuento"][i])]
            get_cue = get_cue.to_dict('records')
            get_cue[0]['_id'] = str(get_cue[0]['_id'])
            b = get_cue[0]
            l_c.append(b)

    if len(resu_query3) > 0:
        d_jue = pd.DataFrame(list(conn.actividades.RecursoJuegos.find()))
        for i in resu_query3.index:
            get_jue = d_jue.loc[d_jue['_id'] == ObjectId(
                resu_query3["recursojuego"][i])]
            get_jue = get_jue.to_dict('records')
            get_jue[0]['_id'] = str(get_jue[0]['_id'])
            c = get_jue[0]
            l_j.append(c)

    if len(resu_query4) > 0:
        d_lib = pd.DataFrame(list(conn.actividades.RecursoLibros.find()))
        for i in resu_query4.index:
            get_lib = d_lib.loc[d_lib['_id'] == ObjectId(
                resu_query4["recursolibro"][i])]
            get_lib = get_lib.to_dict('records')
            get_lib[0]['_id'] = str(get_lib[0]['_id'])
            d = get_lib[0]
            l_l.append(d)

    if len(resu_query5) > 0:
        d_twe = pd.DataFrame(list(conn.actividades.RecursosTweets.find()))
        for i in resu_query5.index:
            get_twe = d_twe.loc[d_twe['_id'] == ObjectId(
                resu_query5["recursotweet"][i])]
            if (get_twe.empty == True):
                pass
            else:
                get_twe = get_twe.to_dict('records')
                get_twe[0]['_id'] = str(get_twe[0]['_id'])
                e = get_twe[0]
                l_t.append(e)

    if len(resu_query6) > 0:
        d_vid = pd.DataFrame(list(conn.actividades.RecursoVideos.find()))
        for i in resu_query6.index:
            get_vid = d_vid.loc[d_vid['_id'] == ObjectId(
                resu_query6["recursovideo"][i])]
            get_vid = get_vid.to_dict('records')
            get_vid[0]['_id'] = str(get_vid[0]['_id'])
            if(isNaN(get_vid[0]['texto'])):
                get_vid[0]['texto'] = ""
            f = get_vid[0]
            l_v.append(f)

    l_g.append(l_a)
    l_g.append(l_c)
    l_g.append(l_j)
    l_g.append(l_l)
    l_g.append(l_t)
    l_g.append(l_v)
    # print(l_g)
    end = time.time()
    print(end - start)
    # print(l_nod_sim)
    return l_g, l_nod_sim
