from fastapi import APIRouter
from config.db import conn
import datetime
from werkzeug.security import generate_password_hash
from fastapi_mail import FastMail, MessageSchema, ConnectionConfig, MessageType
from py2neo import Graph
from starlette.responses import JSONResponse

from graphdatascience import GraphDataScience

user = APIRouter()


@user.get('/users')
async def find_all_user():

    '''a = {
        'cedula': '0105662068',
        'nombre': 'Gabriel',
        'apellido': 'Chuchuca',
        'username': 'gavriellalx@hotmail.com',
        'fechaNacimiento': '1997-09-24',
        'rol': 'admin',
        'password': generate_password_hash('0105662068', 'sha256'),
    }
    id = conn.actividades.user.insert_one(a).inserted_id
    print(id)'''
    l_user = list(conn.actividades.user.find())
    for i, j in enumerate(l_user):
        l_user[i]['_id'] = str(l_user[i]['_id'])
    # print(config('email'))
    return l_user


@user.post('/users')
async def create_user(user: dict):
    # print(user)
    nuevo_user = user

    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    #gds = GraphDataScience("neo4j://repositoriotesisupsvirtual.online:7687",auth=("neo4j", "test"), name="neo4jactividades")
    user_exist = conn.actividades.user.find_one(
        {'cedula': nuevo_user['cedula']})
    if user_exist == None:
        if nuevo_user['rol'] != 'docente':
            if nuevo_user['capacidad_especial'] == False:
                nuevo_user['username'] = nuevo_user['nombre'].lower(
                )+"_"+nuevo_user['apellido'].lower()
                nuevo_user['password'] = nuevo_user['cedula']
                nuevo_user['password'] = generate_password_hash(
                    nuevo_user['password'], 'sha256')
                f = nuevo_user['fechaNacimiento'].split("-")
                nuevo_user['fechaNacimiento'] = datetime.datetime(
                    int(f[0]), int(f[1]), int(f[2]))
                # print(nuevo_user)
                #del nuevo_user['_id']
                id = conn.actividades.user.insert_one(nuevo_user).inserted_id
                print(id)
                user2 = conn.actividades.user.find_one({'_id': id})
                print(user2)

                q_id = "CREATE ("+user2['username'] + \
                    ":User {texto:'"+str(id)+"'})"
                resu_query = gds.run(q_id)

                return {"message": "Usuario creado correctamente"}
            else:
                nuevo_user['username'] = nuevo_user['nombre'].lower(
                )+"_"+nuevo_user['apellido'].lower()
                nuevo_user['password'] = nuevo_user['cedula']
                nuevo_user['password'] = generate_password_hash(
                    nuevo_user['password'], 'sha256')
                f = nuevo_user['fechaNacimiento'].split("-")
                nuevo_user['fechaNacimiento'] = datetime.datetime(
                    int(f[0]), int(f[1]), int(f[2]))
                print(nuevo_user)
                id = conn.actividades.user.insert_one(nuevo_user).inserted_id
                print(id)
                user = conn.actividades.user.find_one({'_id': id})

                q_id = "CREATE ("+user['username'] + \
                    ":User {texto:'"+str(id)+"'})"
                resu_query = gds.run(q_id)
                for dm in nuevo_user['diagnostico_medico']:
                    print(dm)
                    rescp = '''MATCH(n:User),(d:owl__Class)
            where n.texto='%s' AND 
            '%s' IN d.ns1__hasDbXref AND
            not exists((n)-[]->(d))
            create (n)-[:PERSONA_ENF]->(d)
            return True''' % (str(id), dm)
                    resu_query2 = gds.run(rescp)
                #del nuevo_user['_id']
                return {"message": "Usuario creado correctamente"}
        else:
            correo_exi = conn.actividades.user.find_one(
                {'username': nuevo_user['correo']})
            if correo_exi == None:
                nuevo_user['username'] = nuevo_user['correo']

                nuevo_user['password'] = nuevo_user['cedula']
                nuevo_user['password'] = generate_password_hash(
                    nuevo_user['password'], 'sha256')
                f = nuevo_user['fechaNacimiento'].split("-")
                nuevo_user['fechaNacimiento'] = datetime.datetime(
                    int(f[0]), int(f[1]), int(f[2]))
                del nuevo_user['correo']
                # print(nuevo_user)
                # await send_email_async('Usuario creado. Bienvenido al repositorio de recursos educativos', nuevo_user['username'])
                id = conn.actividades.user.insert_one(nuevo_user).inserted_id
                user = conn.actividades.user.find_one({'_id': id})
                return {"message": "Usuario creado correctamente"}
            else:
                return {"message": "El correo ya existe"}
    else:
        return {'message': 'El usuario ya existe'}


conf = ConnectionConfig(
    MAIL_USERNAME="raynpelo@gmail.com",
    MAIL_PASSWORD="fmtrogqbqpqlgnca",
    MAIL_FROM="raynpelo@gmail.com",
    # MAIL_PORT=587,
    # MAIL_SERVER="smtp.gmail.com",
    # MAIL_STARTTLS=True,
    # MAIL_SSL_TLS=False,
    # USE_CREDENTIALS=True,
    # VALIDATE_CERTS=True,
    MAIL_PORT=465,
    MAIL_SERVER="smtp.gmail.com",
    MAIL_STARTTLS=False,
    MAIL_SSL_TLS=True,
    USE_CREDENTIALS=True,
    VALIDATE_CERTS=True

)

# @user.post("/email")
# async def send_file(content: EmailContent):


async def send_email_async(subject: str, email_to: str) -> JSONResponse:
    html = f"""
        {subject}
    """
    print('html', html)
    message = MessageSchema(
        subject=subject,
        recipients=[email_to],
        body=html,
        subtype=MessageType.html
    )
    fm = FastMail(conf)
    await fm.send_message(message)
    return JSONResponse(status_code=200, content={"message": "email has been sent"})


# @user.get('/send-email/asynchronous')
# async def send_email_asynchronous():
#     await send_email_async('Hello World', 'gazir.2409@gmail.com')
#     return 'Success'
