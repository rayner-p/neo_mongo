from fastapi import APIRouter
from py2neo import Graph

icd10 = APIRouter()


@icd10.get('/icd10')
async def get_all_icd10():
    l_icd = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    q_id = "Match(n:owl__Class) WITH n as dat UNWIND dat.ns1__hasDbXref AS codes WITH codes AS code,dat.rdfs__label AS label WHERE code STARTS WITH 'ICD10CM' RETURN code,label order by code ASC"
    resu_query = gds.run(q_id)
    for i in resu_query.index:
        l_icd.append(
            {'code': resu_query["code"][i], 'label': resu_query["label"][i]})
    return l_icd


@icd10.get('/icd10lan')
async def get_all_icd10lan():
    l_icdlan = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    q_id = "Match(n:owl__Class) WITH n as dat UNWIND dat.rdfs__label AS labels UNWIND dat.ns1__hasDbXref AS codes UNWIND dat.ns2__IAO_0000115 AS defi UNWIND ['language','speech','communication'] AS cc WITH codes AS code,defi AS definition,cc AS rescc,labels AS label WHERE definition CONTAINS rescc AND code STARTS WITH 'ICD10CM' RETURN DISTINCT code,label order by code ASC"
    resu_query = gds.run(q_id)
    for i in resu_query.index:
        l_icdlan.append(
            {'code': resu_query["code"][i], 'label': resu_query["label"][i]})
    return l_icdlan
