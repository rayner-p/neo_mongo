#from turtle import pd
from urllib import response
from fastapi import APIRouter
from pymongo import MongoClient
from config.db import conn
#from app.schemas.actividad import actividadesEntity
from py2neo import Graph
from bson.objectid import ObjectId
import time
import pandas as pd
from routes.recurso import isNaN

actividad = APIRouter()


@actividad.get("/actividad/{id}")
async def find_one_acti(id: str):
    start = time.time()
    d_act = pd.DataFrame(list(conn.actividades.libro.find()))
    get_one_act = d_act.loc[d_act['_id'] == ObjectId(id)]
    if(len(get_one_act) > 0):
        get_one_act = get_one_act.to_dict('records')
        get_one_act[0]['_id'] = str(get_one_act[0]['_id'])
        ac = get_one_act[0]
        end = time.time()
        print(end - start)
        return ac
    else:
        return


@actividad.get('/actividades')
async def find_all_activities():
    start = time.time()
    l_a = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    q_id = "match r=((p:Pagina)-[:ACTIVIDAD]->(n:Actividad)) where exists((n)<-[:APPS]-()) OR exists((n)<-[:CUENTOS]-()) OR exists((n)<-[:JUEGOS]-()) OR exists((n)<-[:LIBROS]-()) OR exists((n)<-[:TWEETS]-()) OR exists((n)<-[:VIDEOS]-()) return n.texto as texto ORDER by p.pagina ASC"
    resu_query = gds.run(q_id).to_data_frame()
    d_act = pd.DataFrame(list(conn.actividades.libro.find()))
    for i in resu_query.iloc[:, 0]:
        get_act = d_act.loc[d_act['_id'] == ObjectId(i)]
        if (get_act.empty == True):
            pass
        else:
            get_act = get_act.to_dict('records')
            get_act[0]['_id'] = str(get_act[0]['_id'])
            l_a.append(get_act[0])

    end = time.time()
    print(end - start)
    return l_a


@actividad.get("/actividadesrecomendadas")
async def acti_reco():
    start = time.time()
    l_a_r = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    try:
        cls = "CALL gds.graph.drop('prob') YIELD graphName;"
        degree_cls = gds.run(cls)
    except Exception as e:
        pass
    q = gds.run(
        "CALL gds.graph.project('prob', 'Actividad', 'SIMILARIDAD', {relationshipProperties: 'coseno'})")
    q1 = gds.run("CALL gds.pageRank.stream('prob') YIELD nodeId, score RETURN gds.util.asNode(nodeId).texto AS name, score ORDER BY score DESC, name ASC").to_data_frame()
    d_act = pd.DataFrame(list(conn.actividades.libro.find()))
    c = 0
    l_id = []
    for i in q1.iloc[:, 0]:
        #print('iiii', i)
        query = gds.run("match (n:Actividad),(e:owl__Class) where n.texto='"+i +
                        "'and not exists((n)-[]->(e)) and exists((n)<-[:APPS|:CUENTOS|:JUEGOS|:LIBROS|:TWEETS|:VIDEOS]-()) return n.texto").to_data_frame()
        #print('aaa', query)
        if(len(query.to_dict('records')) > 0):
            print('entra')
            # print(query.to_dict('records'))
            get_act = d_act.loc[d_act['_id'] == ObjectId(i)]
            get_act = get_act.to_dict('records')
            if(len(get_act) > 0):
                get_act[0]['_id'] = str(get_act[0]['_id'])
                # print(get_act)
                l_id.append(get_act[0])
                c += 1
                if(c == 5):
                    break

    end = time.time()
    print(end - start)
    return l_id


@actividad.post("/actividadesrecomendadas_d")
async def acti_reco_d(idc10: list):
    conn = MongoClient(
        "mongodb://repoups:p4t1t0.123@repositoriotesisupsvirtual.online:27017?authMechanism=DEFAULT")
    start = time.time()
    l_a_r = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    try:
        cls = "CALL gds.graph.drop('prob') YIELD graphName;"
        degree_cls = gds.run(cls)
    except Exception as e:
        pass
    q = gds.run(
        "CALL gds.graph.project('prob', 'Actividad', 'SIMILARIDAD', {relationshipProperties: 'coseno'})")
    q1 = gds.run(
        "CALL gds.pageRank.stream('prob') YIELD nodeId, score RETURN gds.util.asNode(nodeId).texto AS name, score ORDER BY score DESC, name ASC")
    # print(q1)
    l_id = []
    res_algo = q1.to_dict('records')
    # print(len(res_algo))
    for l in idc10:
        cc = 0
        print(l)
        for i in res_algo:
            query = gds.run("match (n:Actividad),(e:owl__Class) where n.texto='" +
                            i['name']+"' and '"+l+"' IN e.ns1__hasDbXref and exists((n)-[]->(e)) and exists((n)<-[:APPS|:CUENTOS|:JUEGOS|:LIBROS|:TWEETS|:VIDEOS]-()) return n")
            # print(len(query))
            if len(query) > 0:
                act = conn.actividades.ActividadesCapacidades.find_one(
                    {"_id": ObjectId(str(i['name']))})
                if act:
                    # print(i['name'])
                    # print(act)
                    act['_id'] = str(act['_id'])
                    cc += 1
                    l_id.append(act)
                    if cc == 3:
                        break
    end = time.time()
    print('l_id', l_id)
    print(end - start)
    return l_id


@actividad.post('/actividades_d')
async def set_act_dis(id_e: str):
    start = time.time()
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    d_act_d = pd.DataFrame(
        list(conn.actividades.ActividadesCapacidades.find()))
    resu_query = gds.run(
        "MATCH p=(a:Actividad)-[r:ACTIVIDAD_ENF]->(n:owl__Class) where '"+id_e+"' IN n.ns1__hasDbXref  RETURN a.texto")
    l_a_d = []
    for j in resu_query.iloc[:, 0]:
        get_act_d = d_act_d.loc[d_act_d['_id'] == ObjectId(j)]
        if(len(get_act_d) > 0):
            get_act_d = get_act_d.to_dict('records')
            get_act_d[0]['_id'] = str(get_act_d[0]['_id'])
            try:
                if(isNaN(get_act_d[0]['titulo'])):
                    get_act_d[0]['titulo'] = ""
            except:
                pass
            l_a_d.append(get_act_d[0])
    end = time.time()
    print(end - start)
    return l_a_d


@actividad.get("/actividad_d/{id}")
async def find_one_acti_disc(id: str):
    start = time.time()
    d_act_dis = pd.DataFrame(
        list(conn.actividades.ActividadesCapacidades.find()))
    get_one_act_dis = d_act_dis.loc[d_act_dis['_id'] == ObjectId(id)]
    if(len(get_one_act_dis) > 0):
        get_one_act_dis = get_one_act_dis.to_dict('records')
        get_one_act_dis[0]['_id'] = str(get_one_act_dis[0]['_id'])
        try:
            if(isNaN(get_one_act_dis[0]['titulo'])):
                get_one_act_dis[0]['titulo'] = ""
        except:
            pass
        ad = get_one_act_dis[0]
        end = time.time()
        print(end - start)
        return ad
    else:
        return


@actividad.get("/actividades_d")
async def find_all_activities_special():
    get_act_dis = list(conn.actividades.ActividadesCapacidades.find())
    #get_act_dis[0]['_id'] = str(get_act_dis[0]['_id'])
    # print(get_act_dis)
    for i in get_act_dis:
        i['_id'] = str(i['_id'])
        # print(i['_id'])

    return get_act_dis


def isNaN(num):
    #print(num!= num)
    return num != num


@actividad.get("/actividades_reci")
async def get_acti_rece():
    start = time.time()
    l_a_r = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    q_id = "MATCH (n:Actividad) where exists((n)<-[:APPS]-()) OR exists((n)<-[:CUENTOS]-()) OR exists((n)<-[:JUEGOS]-()) OR exists((n)<-[:LIBROS]-()) OR exists((n)<-[:TWEETS]-()) OR exists((n)<-[:VIDEOS]-()) return n.texto"
    resu_query = gds.run(q_id).to_data_frame()
    d_acti_reci = pd.DataFrame(list(conn.actividades.ActividadesN.find()))
    for i in resu_query.iloc[:, 0]:
        get_acti_reci = d_acti_reci.loc[d_acti_reci['_id'] == ObjectId(i)]
        get_acti_reci = get_acti_reci.to_dict('records')
        if(len(get_acti_reci) > 0):
            get_acti_reci[0]['_id'] = str(get_acti_reci[0]['_id'])
            if(isNaN(get_acti_reci[0]['indicadores']) == False):
                get_acti_reci[0]['indicadores'] = str(
                    get_acti_reci[0]['indicadores'])
                try:
                    if(isNaN(get_acti_reci[0]['Area'])):
                        get_acti_reci[0]['Area'] = ""
                    if(isNaN(get_acti_reci[0]['Bloque'])):
                        get_acti_reci[0]['Bloque'] = ""
                    if(isNaN(get_acti_reci[0]['Competencia'])):
                        get_acti_reci[0]['Competencia'] = ""
                except:
                    pass
                l_a_r.append(get_acti_reci[0])
            else:
                get_acti_reci[0]['Area'] = str(get_acti_reci[0]['Area'])
                get_acti_reci[0]['Bloque'] = str(get_acti_reci[0]['Bloque'])
                get_acti_reci[0]['Competencia'] = str(
                    get_acti_reci[0]['Competencia'])
                try:
                    if(isNaN(get_acti_reci[0]['indicadores'])):
                        get_acti_reci[0]['indicadores'] = ""
                except:
                    pass
                l_a_r.append(get_acti_reci[0])
    end = time.time()
    print(end - start)
    return l_a_r


@actividad.get("/actividades_disc_reci")
async def get_acti_disc_rece():
    start = time.time()
    l_a_d_r = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    q_id = "MATCH (n:Actividad) where exists((n)<-[:APPS]-()) OR exists((n)<-[:CUENTOS]-()) OR exists((n)<-[:JUEGOS]-()) OR exists((n)<-[:LIBROS]-()) OR exists((n)<-[:TWEETS]-()) OR exists((n)<-[:VIDEOS]-()) return n.texto"
    resu_query = gds.run(q_id).to_data_frame()
    #print(resu_query.iloc[:, 0])
    d_acti_disc_reci = pd.DataFrame(
        list(conn.actividades.ActividadesND.find()))
    # print(d_acti_reci)
    for i in resu_query.iloc[:, 0]:
        # print(i)
        get_acti_disc_reci = d_acti_disc_reci.loc[d_acti_disc_reci['_id'] == ObjectId(
            i)]
        # print(get_acti_reci)
        get_acti_disc_reci = get_acti_disc_reci.to_dict('records')
        # print(get_acti_reci)
        if(len(get_acti_disc_reci) > 0):
            # print(get_acti_reci[0]['Area'])
            get_acti_disc_reci[0]['_id'] = str(get_acti_disc_reci[0]['_id'])
            if(isNaN(get_acti_disc_reci[0]['indicadores']) == False):
                get_acti_disc_reci[0]['indicadores'] = str(
                    get_acti_disc_reci[0]['indicadores'])
                try:
                    if(isNaN(get_acti_disc_reci[0]['Area'])):
                        get_acti_disc_reci[0]['Area'] = ""
                    if(isNaN(get_acti_disc_reci[0]['Bloque'])):
                        get_acti_disc_reci[0]['Bloque'] = ""
                    if(isNaN(get_acti_disc_reci[0]['Competencia'])):
                        get_acti_disc_reci[0]['Competencia'] = ""
                    if(isNaN(get_acti_disc_reci[0]['codigo'])):
                        get_acti_disc_reci[0]['codigo'] = ""
                except:
                    pass
                l_a_d_r.append(get_acti_disc_reci[0])
            else:
                get_acti_disc_reci[0]['Area'] = str(
                    get_acti_disc_reci[0]['Area'])
                get_acti_disc_reci[0]['Bloque'] = str(
                    get_acti_disc_reci[0]['Bloque'])
                get_acti_disc_reci[0]['Competencia'] = str(
                    get_acti_disc_reci[0]['Competencia'])
                try:
                    if(isNaN(get_acti_disc_reci[0]['indicadores'])):
                        get_acti_disc_reci[0]['indicadores'] = ""
                    if(isNaN(get_acti_disc_reci[0]['codigo'])):
                        get_acti_disc_reci[0]['codigo'] = ""
                except:
                    pass
                l_a_d_r.append(get_acti_disc_reci[0])
    end = time.time()
    print(end - start)
    return l_a_d_r


@actividad.post("/actividades_disc_reci")
async def set_acti_disc_rece(ids: list):
    start = time.time()
    l_a_d_r = []
    gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                name="neo4jarticulos2", auth=("neo4j", "test"))
    for id in ids:
        q_id = "match (n:Actividad),(e:owl__Class) where '"+id + \
            "' IN e.ns1__hasDbXref and exists((n)-[]->(e)) and exists((n)<-[:APPS|:CUENTOS|:JUEGOS|:LIBROS|:TWEETS|:VIDEOS]-()) return n.texto"
        resu_query = gds.run(q_id)
        #print(resu_query.iloc[:, 0])
        d_acti_disc_reci = pd.DataFrame(
            list(conn.actividades.ActividadesND.find()))
        # print(d_acti_reci)
        for i in resu_query.iloc[:, 0]:
            # print(i)
            get_acti_disc_reci = d_acti_disc_reci.loc[d_acti_disc_reci['_id'] == ObjectId(
                i)]
            # print(get_acti_reci)
            get_acti_disc_reci = get_acti_disc_reci.to_dict('records')
            # print(get_acti_reci)
            if(len(get_acti_disc_reci) > 0):
                # print(get_acti_reci[0]['Area'])
                get_acti_disc_reci[0]['_id'] = str(
                    get_acti_disc_reci[0]['_id'])
                if(isNaN(get_acti_disc_reci[0]['indicadores']) == False):
                    get_acti_disc_reci[0]['indicadores'] = str(
                        get_acti_disc_reci[0]['indicadores'])
                    try:
                        if(isNaN(get_acti_disc_reci[0]['Area'])):
                            get_acti_disc_reci[0]['Area'] = ""
                        if(isNaN(get_acti_disc_reci[0]['Bloque'])):
                            get_acti_disc_reci[0]['Bloque'] = ""
                        if(isNaN(get_acti_disc_reci[0]['Competencia'])):
                            get_acti_disc_reci[0]['Competencia'] = ""
                        if(isNaN(get_acti_disc_reci[0]['codigo'])):
                            get_acti_disc_reci[0]['codigo'] = ""
                    except:
                        pass
                    l_a_d_r.append(get_acti_disc_reci[0])
                else:
                    get_acti_disc_reci[0]['Area'] = str(
                        get_acti_disc_reci[0]['Area'])
                    get_acti_disc_reci[0]['Bloque'] = str(
                        get_acti_disc_reci[0]['Bloque'])
                    get_acti_disc_reci[0]['Competencia'] = str(
                        get_acti_disc_reci[0]['Competencia'])
                    try:
                        if(isNaN(get_acti_disc_reci[0]['indicadores'])):
                            get_acti_disc_reci[0]['indicadores'] = ""
                        if(isNaN(get_acti_disc_reci[0]['codigo'])):
                            get_acti_disc_reci[0]['codigo'] = ""
                    except:
                        pass
                    l_a_d_r.append(get_acti_disc_reci[0])
    end = time.time()
    print(end - start)
    return l_a_d_r


@actividad.get("/acti_reci/{id}")
async def find_one_acti_reci(id: str):
    start = time.time()
    d_act = pd.DataFrame(list(conn.actividades.ActividadesN.find()))
    get_one_act_reci = d_act.loc[d_act['_id'] == ObjectId(id)]
    if(len(get_one_act_reci) > 0):
        get_one_act_reci = get_one_act_reci.to_dict('records')
        get_one_act_reci[0]['_id'] = str(get_one_act_reci[0]['_id'])
        try:
            if(isNaN(get_one_act_reci[0]['indicadores'])):
                get_one_act_reci[0]['indicadores'] = ""
            else:
                get_one_act_reci[0]['indicadores'] = str(
                    get_one_act_reci[0]['indicadores'])
        except:
            pass

        try:
            if(isNaN(get_one_act_reci[0]['Area'])):
                get_one_act_reci[0]['Area'] = ""
            else:
                get_one_act_reci[0]['Area'] = str(get_one_act_reci[0]['Area'])
            if(isNaN(get_one_act_reci[0]['Bloque'])):
                get_one_act_reci[0]['Bloque'] = ""
            else:
                get_one_act_reci[0]['Bloque'] = str(
                    get_one_act_reci[0]['Bloque'])
            if(isNaN(get_one_act_reci[0]['Competencia'])):
                get_one_act_reci[0]['Competencia'] = ""
            else:
                get_one_act_reci[0]['Competencia'] = str(
                    get_one_act_reci[0]['Competencia'])
        except:
            pass
        ac = get_one_act_reci[0]
        end = time.time()
        print(end - start)
        return ac
    else:
        return


@actividad.get("/acti_espe_reci/{id}")
async def find_one_acti_espe_reci(id: str):
    start = time.time()
    d_act = pd.DataFrame(list(conn.actividades.ActividadesND.find()))
    get_one_act_espe_reci = d_act.loc[d_act['_id'] == ObjectId(id)]
    if(len(get_one_act_espe_reci) > 0):
        get_one_act_espe_reci = get_one_act_espe_reci.to_dict('records')
        get_one_act_espe_reci[0]['_id'] = str(get_one_act_espe_reci[0]['_id'])
        try:
            if(isNaN(get_one_act_espe_reci[0]['indicadores'])):
                get_one_act_espe_reci[0]['indicadores'] = ""
            else:
                get_one_act_espe_reci[0]['indicadores'] = str(
                    get_one_act_espe_reci[0]['indicadores'])
            if(isNaN(get_one_act_espe_reci[0]['Area'])):
                get_one_act_espe_reci[0]['Area'] = ""
            else:
                get_one_act_espe_reci[0]['Area'] = str(
                    get_one_act_espe_reci[0]['Area'])
            if(isNaN(get_one_act_espe_reci[0]['Bloque'])):
                get_one_act_espe_reci[0]['Bloque'] = ""
            else:
                get_one_act_espe_reci[0]['Bloque'] = str(
                    get_one_act_espe_reci[0]['Bloque'])
            if(isNaN(get_one_act_espe_reci[0]['Competencia'])):
                get_one_act_espe_reci[0]['Competencia'] = ""
            else:
                get_one_act_espe_reci[0]['Competencia'] = str(
                    get_one_act_espe_reci[0]['Competencia'])
            if(isNaN(get_one_act_espe_reci[0]['codigo'])):
                get_one_act_espe_reci[0]['codigo'] = ""
        except:
            pass
        ac = get_one_act_espe_reci[0]
        end = time.time()
        print(end - start)
        return ac
    else:
        return


@actividad.get("/acti_leng")
async def actividad_lenguaje():
    start = time.time()
    res_act = []
    palabra = "lenguaje"
    conn = MongoClient(
        "mongodb://repoups:p4t1t0.123@repositoriotesisupsvirtual.online:27017?authMechanism=DEFAULT")
    res_habilidades = (pd.DataFrame(
        list(conn.actividades.NewActividades.find()))).to_dict('records')
    for act in res_habilidades:
        if palabra in act['Habilidad']:
            act['_id'] = str(act['_id'])
            act['Area'] = str(act['Area'])
            act['Bloque'] = str(act['Bloque'])
            act['Competencia'] = str(act['Competencia'])
            res_act.append(act)
    end = time.time()
    print(end - start)
    return res_act
