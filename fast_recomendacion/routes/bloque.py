from fastapi import APIRouter
from config.db import conn
import time

bloque = APIRouter()


@bloque.get('/bloques')
async def get_all_bloque():
    start = time.time()
    l_bloq = list(conn.actividades.Bloque.find())
    for i, j in enumerate(l_bloq):
        l_bloq[i]['_id'] = str(l_bloq[i]['_id'])
        l_bloq[i]['Area'] = str(l_bloq[i]['Area'])
    end = time.time()
    print(end - start)
    return l_bloq
