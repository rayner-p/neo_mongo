from fastapi import APIRouter
from config.db import conn
import time

competencia = APIRouter()


@competencia.get('/competencias')
async def get_all_competencia():
    start = time.time()
    l_comp = list(conn.actividades.Competencia.find())
    for i, j in enumerate(l_comp):
        l_comp[i]['_id'] = str(l_comp[i]['_id'])
        l_comp[i]['Bloque'] = str(l_comp[i]['Bloque'])
    end = time.time()
    print(end - start)
    return l_comp
