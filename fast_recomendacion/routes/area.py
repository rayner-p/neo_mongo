from fastapi import APIRouter
from config.db import conn
import time

area = APIRouter()


@area.get('/areas')
async def get_all_area():
    start = time.time()
    l_area = list(conn.actividades.Area.find())
    for i, j in enumerate(l_area):
        l_area[i]['_id'] = str(l_area[i]['_id'])
    # print(l_area)
    end = time.time()
    print(end - start)
    return l_area
    # return areasEntity(conn.TestTesis1.Area.find())

    # return area
