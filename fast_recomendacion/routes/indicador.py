from fastapi import APIRouter
from config.db import conn
from routes.recurso import isNaN

idcd = APIRouter()


@idcd.get('/indicadores')
async def get_all_indicadores():
    d_indi = list(conn.actividades.Indicadores.find())
    for i, j in enumerate(d_indi):
        d_indi[i]['_id'] = str(d_indi[i]['_id'])
    return d_indi
