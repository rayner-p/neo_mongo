# -*- coding: utf-8 -*-
from zipfile import ZipFile
from fastapi import APIRouter
from config.db import conn
from starlette.responses import JSONResponse

import time
import logging
from lib2to3.pgen2 import driver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common import keys
from selenium.common.exceptions import (ElementNotSelectableException,
                                        ElementNotVisibleException,
                                        NoSuchElementException,
                                        NoSuchFrameException,
                                        NoSuchWindowException,
                                        TimeoutException, WebDriverException)
from selenium import webdriver
from traceback import print_tb
from datetime import datetime
import time
import os
from fastapi_mail import FastMail, MessageSchema, ConnectionConfig, MessageType
from fastapi import UploadFile, File

objetoActivity = APIRouter()


#consa = os.system("""osascript -e 'tell app "exe" to open'""")
class seleniumElearning():
    def requisitosConexion():
        # os.path.abspath(os.path.dirname(__file__)) os.path.join(PROJECT_ROOT, "chromedriver")
        PROJECT_ROOT = os.path.dirname(os.path.abspath("__file__"))
        DRIVER_BIN = os.popen('which chromedriver').read().strip()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--disable-popup-blocking")
        chrome_options.add_experimental_option("detach", True)
        driver = webdriver.Chrome(executable_path=DRIVER_BIN)
        return driver

    def aniadirNuevaPagina():
        datos_aniadir_pagina = {"boton_aniadir_pagina": "/html/body/div[3]/div/div[1]/div/div[1]/div/div/div[1]/em/button/span[1]",
                                "seleccion_texto_inicio": "/html/body/div[3]/div/div[1]/div/div[2]/div[3]/div/table/tbody/tr[2]/td/div"}
        return datos_aniadir_pagina

    def accederObjetoAprendizaje(driver):
        datos_pagina = seleniumElearning.aniadirNuevaPagina()
        navegar_nuevo_ova = "/html/body/p/a"
        #driver = seleniumElearning.requisitosConexion()
        url = 'http://localhost:51235/newPackage'
        driver.get(url)
        seleccionar = driver.find_element(By.XPATH, navegar_nuevo_ova)
        seleccionar.click()
        time.sleep(5)
        prueba = driver.find_element(
            By.XPATH, datos_pagina['boton_aniadir_pagina']).click()
        time.sleep(5)

    def seleccionarObjetoTexto(driver):
        # /html/body/div[3]/div/div[3]/div[3]/div/table/tbody/tr[3]/td/table/tbody/tr[2]/td[1]
        seleccion_boton_agregar_texto = "/html/body/div[3]/div/div[3]/div[3]/div/table/tbody/tr[3]/td/table/tbody/tr[2]/td[1]/div"
        seleccion_boton_agregar_texto_fijo = driver.find_element(
            By.XPATH, seleccion_boton_agregar_texto).click()
        return seleccion_boton_agregar_texto_fijo

    def verifcarIframePrincipal(driver):
        #driver = seleniumElearning.requisitosConexion()
        espera_verificar_conocimiento_prv = WebDriverWait(
            driver,
            15,
            poll_frequency=10,
            ignored_exceptions=[
                ElementNotVisibleException, ElementNotSelectableException
            ])
        try:
            verificar_disponibilidd_Iframe = espera_verificar_conocimiento_prv.until(
                ec.frame_to_be_available_and_switch_to_it("authoringIFrame1-frame"))
        except NoSuchFrameException as exception:
            print("No se ha encontrado el Iframe principal para el caso practico")

    def regresarVentanaPrincipal(driver):
        #driver = seleniumElearning.requisitosConexion()
        time.sleep(2)
        wait = WebDriverWait(driver, 5)
        try:
            regresar_ventana_principal = driver.switch_to.default_content()
            print("regresa ventana")
        except NoSuchWindowException as exception:
            print("No existe ventana")
        return regresar_ventana_principal

    def pathRenombrarPropiedades():
        boton_renombrar = "button-2536-btnEl"
        seleccion_cuadro_texto_renombrar = "#messagebox-1001-testfield-inputEl"
        selecion_elemento_renombrar = "#messagebox-1001-testfield-inputEl"
        seleccion_boton_aceptar_renombrar = "button-1005-btnInnerEl"
        return boton_renombrar, seleccion_cuadro_texto_renombrar, selecion_elemento_renombrar, seleccion_boton_aceptar_renombrar

    def renombrarTextoPropiedades(driver, nuevo_nombre_texto):
        opciones_renombrar = seleniumElearning.pathRenombrarPropiedades()
        # renombramos texto
        boton_renombrar_texto = driver.find_element(
            By.ID, opciones_renombrar[0]).click()
        # buscamos la nueva ventana que se abre para escribir el nombre del nuevo tema y mandamos a limipar el texto existente
        seleccion_input_renombrar_texto = driver.find_element(
            By.CSS_SELECTOR, opciones_renombrar[1]).clear()
        # Limpiado el txto existente buscamos otra vez el elemento para mandar a escribir el nuevo texto
        seleccion_input_renombrar_texto = driver.find_element(
            By.CSS_SELECTOR, opciones_renombrar[2]).send_keys(nuevo_nombre_texto)
        boton_acepta_renombrar_texto = driver.find_element(
            By.ID, opciones_renombrar[3]).click()
        print("RESULTADO DEL BOTON", boton_acepta_renombrar_texto)

    def guardarArchivo(driver, nombre):
        nombre_archivo_guardar = nombre
        opcion_path_guardar_archivo = "html"
        seleccion_ubicacion_carpeta_personal = "/html/body/div[11]/div[2]/div/div/div[3]/em/button"
        seleccion_ubicacion_carpeta_descarga = "/html/body/div[11]/div[3]/div[1]/div[2]/div/table/tbody/tr[28]/td/div"
        seleccion_ubicacion_guardar_archivo = "/html/body/div[11]/div[3]/div[1]/div[2]/div/table/tbody/tr[36]/td/div"
        seleccion_cuadro_nombre_archivo_guardar = "/html/body/div[11]/table[2]/tbody/tr/td[2]/table/tbody/tr/td[1]"
        seleccion_nombre_archivo_guardar = "/html/body/div[9]/table[2]/tbody/tr/td[2]/table/tbody/tr/td[1]/input"

        seleccion_boton_guardar_archivo_escritorio = '/html/body/div[11]/div[4]/div/div/div[3]/em/button'
        seleccion_boton_borrar = "#button-2535-btnEl"
        seleccion_boton_aceptar_borrar = "#button-1006-btnEl"
        seleccion_aceptar_boton_final_guardar_archivo = '/html/body/div[11]/div[3]/div/div/div[1]/em/button'
        driver.find_element(By.CSS_SELECTOR,
                            opcion_path_guardar_archivo).send_keys(Keys.CONTROL + "S")
        time.sleep(3)
        carpeta = driver.find_elements(
            By.XPATH, seleccion_ubicacion_guardar_archivo)
        time.sleep(5)
        nombre_seleccion = driver.find_element(
            By.XPATH, seleccion_cuadro_nombre_archivo_guardar)
        print("textoooooo")
        ingresarTexto = driver.find_element(
            By.XPATH, "/html/body/div[11]/table[2]/tbody/tr/td[2]/table/tbody/tr/td[1]/input").send_keys(nombre_archivo_guardar)
        time.sleep(2)
        seleccion_boton_guardar = driver.find_element(
            By.XPATH, seleccion_boton_guardar_archivo_escritorio).click()
        time.sleep(2)
        print("GUARDADO", seleccion_boton_guardar)
        seleccion_aceptar_guardado_archivo = driver.find_element(
            By.XPATH, seleccion_aceptar_boton_final_guardar_archivo).click()
        print("GUARDADO FINAL", seleccion_aceptar_guardado_archivo)
        driver.refresh()

    def atributosTextoTitulo():

        selector_insertar_titulo = "/html/body/form/div[1]/div[2]/div/input[2]"
        selector_insertar_texto = "/html/body"
        boton_hecho = "/html/body/form/div[1]/div[2]/div/p/span/a/img"
        selector_iframe_texto_parrafo = "/html/body/form/div[1]/div[2]/div/div[2]/div/div/div[2]/iframe"
        return selector_insertar_titulo, selector_insertar_texto, boton_hecho, selector_iframe_texto_parrafo

    def atributosTextoActividad():
        lista_path_actividades = {"seleccion_iframe_actividad": "/html/body/form/div[1]/div[2]/div/div[2]/div/div/div[2]/iframe", "seleccion_boton_texto_actividad": "/html/body/div[3]/div/div[3]/div[3]/div/table/tbody/tr[3]/td/table/tbody/tr[3]/td[1]",
                                  "seleccion_buscar_agregar_texto_actividad": "title1", "seleccion_parrafo_texto_actividad": "/html/body", "boton_hecho_activdad": "#exe-submitButton > a > img"}
        return lista_path_actividades


class seleniumElearningPropiedades():

    def agregarInformacionTexto(self, tituloTexto, parrafoTexto):
        driver = seleniumElearning.requisitosConexion()
        print(driver)
        self.driver = driver
        boton_acceder_nueva_pagian = seleniumElearning.accederObjetoAprendizaje(
            driver)
        seleniumElearning.seleccionarObjetoTexto(driver)
        verificarIframe1 = seleniumElearning.verifcarIframePrincipal(driver)
        wait = WebDriverWait(driver, 5)
        selector_insertar_titulo = seleniumElearning.atributosTextoTitulo()
        buscarTitulo = driver.find_element(
            By.XPATH, selector_insertar_titulo[0]).send_keys(tituloTexto)
        time.sleep(2)
        verificar_disponibilidd_Iframe2 = wait.until(ec.frame_to_be_available_and_switch_to_it(
            driver.find_element(By.XPATH, selector_insertar_titulo[3])))
        ingresarParrafo = driver.find_element(
            By.XPATH, selector_insertar_titulo[1]).send_keys(parrafoTexto)
        time.sleep(2)
        seleniumElearning.regresarVentanaPrincipal(driver)
        seleniumElearning.verifcarIframePrincipal(driver)
        boton_confirmar_agregar_texto = driver.find_element(
            By.XPATH, selector_insertar_titulo[2]).click()
        time.sleep(2)
        # Salimos el iframe y regresamos a la vntana principal
        seleniumElearning.regresarVentanaPrincipal(driver)
        seleniumElearning.renombrarTextoPropiedades(driver, tituloTexto)
        #seleniumElearning.guardarArchivo(driver, "prueba_nueva13")
        # driver.get_cookies()

    def agregarInformacionActividad(self, textoActividad, texto_parrafo_actividad):
        driver = self.driver
        lista_datos_propiedades_actividad = seleniumElearning.atributosTextoActividad()
        time.sleep(2)
        try:
            seleccion_aniadir_pagina2 = driver.find_element(
                By.XPATH, lista_datos_propiedades_actividad['seleccion_boton_texto_actividad']).click()
            time.sleep(3)
            print('driver', seleccion_aniadir_pagina2)
            seleniumElearning.verifcarIframePrincipal(driver)

            wait6 = WebDriverWait(driver,
                                  10,
                                  poll_frequency=10,
                                  ignored_exceptions=[
                                      ElementNotVisibleException,
                                      ElementNotSelectableException
                                  ])
            try:
                buscarTitulo_Actividad = driver.find_element(
                    By.ID, lista_datos_propiedades_actividad['seleccion_buscar_agregar_texto_actividad']).send_keys(textoActividad)
                verificar_disponibilidd_Iframe2 = wait6.until(
                    ec.frame_to_be_available_and_switch_to_it(
                        driver.find_element(
                            By.XPATH, lista_datos_propiedades_actividad['seleccion_iframe_actividad']
                        )))
                ingresar_texto_parrafo_actividad = driver.find_element(
                    By.XPATH, lista_datos_propiedades_actividad['seleccion_parrafo_texto_actividad']).send_keys(
                        texto_parrafo_actividad)
                seleniumElearning.regresarVentanaPrincipal(driver)
                print("encotnrdo texto acividad",
                      ingresar_texto_parrafo_actividad)
                # acceder al Iframe de la actividad a crear
                seleniumElearning.verifcarIframePrincipal(driver)
                boton_confirmar_agregar_ac = driver.find_element(
                    By.CSS_SELECTOR, lista_datos_propiedades_actividad['boton_hecho_activdad']).click()
                print("done activity")
            except NoSuchFrameException as exception:
                print("No se ha podido encontrar el Iframe de la actividad")

        except NoSuchElementException as exception:
            print("El Iframe principal no puede ser encontrado")

        WebDriverWait(driver, 5)
        try:
            seleniumElearning.regresarVentanaPrincipal(driver)
        except NoSuchWindowException as exception:
            print("no se ha encontrado el elemento")

    def agregarBibliografia(self, textoBibliografia):
        driver = self.driver
        try:

            datos_aniador = seleniumElearning.aniadirNuevaPagina()
            driver.find_element(
                By.XPATH, datos_aniador['seleccion_texto_inicio']).click()
            WebDriverWait(driver, 4)
            driver.find_element(
                By.XPATH, datos_aniador['boton_aniadir_pagina']).click()
        except NoSuchElementException as excp:
            logging.error({'No se encuentran los elementos del path'})

        try:
            seleniumElearning.seleccionarObjetoTexto(driver)
        except:
            logging.error({'No se encuentran los elementos del path'})

        try:
            seleniumElearning.verifcarIframePrincipal(driver)
        except NoSuchFrameException as exception:
            logging.error({'No se pudo regresar a la ventana principal'})

        wait = WebDriverWait(driver, 5)
        try:
            selector_insertar_titulo = seleniumElearning.atributosTextoTitulo()
        except NoSuchElementException as error:
            logging.info(
                {f'Ha ocurrido un error al obtener los path del texto': {error}})
        WebDriverWait(driver, 5)
        verificar_disponibilidd_Iframe2 = wait.until(ec.frame_to_be_available_and_switch_to_it(
            driver.find_element(By.XPATH, selector_insertar_titulo[3])))

        links_bibliografia = []
        for datos in range(len(textoBibliografia)):
            links_bibliografia.append(textoBibliografia[datos])
        lines = "\n".join(links_bibliografia)
        ingresarParrafo = driver.find_element(
            By.XPATH, selector_insertar_titulo[1]).send_keys(lines, "\n")
        WebDriverWait(driver, 5)
        try:
            seleniumElearning.regresarVentanaPrincipal(driver)
        except NoSuchWindowException as exception:
            logging.exception({'No se pudo regresar a la ventana principal'})
        try:
            seleniumElearning.verifcarIframePrincipal(driver)
        except NoSuchFrameException as exception:
            logging.error({'No se pudo regresar a la ventana principal'})
        boton_confirmar_agregar_texto = driver.find_element(
            By.XPATH, selector_insertar_titulo[2]).click()
        WebDriverWait(driver, 5)
        # Salimos el iframe y regresamos a la vntana principal
        try:
            seleniumElearning.regresarVentanaPrincipal(driver)
        except NoSuchWindowException as exception:
            logging.exception({'No se pudo regresar a la ventana principal'})
        try:
            seleniumElearning.renombrarTextoPropiedades(driver, "Bibliografia")
        except NoSuchElementException as err:
            logging.warning(
                {f'!Ups!...Ocurrió un error al momento de renombrar el texto de la bibliografia'})


# @objetoActivity.get('/objetoAprendizaje{tituloTexto}{parrafoTexto}{tituloActividad}{parrafoActividad}{links}')
# def objetoAprendizaje(tituloTexto,
#                       parrafoTexto,
#                       tituloActividad,
#                       parrafoActividad,
#                       links):
#     print("llegan al fast{{tituloActividad}}")
#     s = seleniumElearningPropiedades()
#     s.agregarInformacionTexto(tituloTexto, parrafoTexto)
#     s.agregarInformacionActividad(tituloActividad, parrafoActividad)
#     s.agregarBibliografia(links)

#     return "done"

@objetoActivity.get('/objetoAprendizaje/{tituloTexto}/{parrafoTexto}/{tituloActividad}/{parrafoActividad}/{links}/{user}')
async def objetoAprendizaje(tituloTexto,
                            parrafoTexto,
                            tituloActividad,
                            parrafoActividad,
                            links, user):
    print("llegan al fast,{tituloActividad}", user)

    s = seleniumElearningPropiedades()
    s.agregarInformacionTexto(tituloTexto, parrafoTexto)
    s.agregarInformacionActividad(tituloActividad, parrafoActividad)
    s.agregarBibliografia(links)
    await send_email_async(tituloTexto, "Saludos. Su Objeto de Aprendizaje ha sido creado exitosamente", user)
    time.sleep(5)
    os.system(""" osascript -e 'tell app "exe" to close' """)
    return "done"


conf = ConnectionConfig(
    MAIL_USERNAME="raynpelo@gmail.com",
    MAIL_PASSWORD="fmtrogqbqpqlgnca",
    MAIL_FROM="raynpelo@gmail.com",
    # MAIL_PORT=587,
    # MAIL_SERVER="smtp.gmail.com",
    # MAIL_STARTTLS=True,
    # MAIL_SSL_TLS=False,
    # USE_CREDENTIALS=True,
    # VALIDATE_CERTS=True,
    MAIL_PORT=465,
    MAIL_SERVER="smtp.gmail.com",
    MAIL_STARTTLS=False,
    MAIL_SSL_TLS=True,
    USE_CREDENTIALS=True,
    VALIDATE_CERTS=True

)


@objetoActivity.post("/email/{title_docuent}/{subject}/{email_to}")
async def send_email_async(title_docuent, subject: str, email_to: str) -> JSONResponse:
    email_to = email_to.replace("[object Object]", '')
    title_docuentss = title_docuent.replace(" ", "_")
    title_docuents = title_docuent+".elp"
    html = f"""
        {subject}
    """
    fns = os.path.basename(
        '/Users/rayner/Downloads/ExeLearning/'+title_docuents)

    attachmen = ZipFile(title_docuentss, "zip", 'w')
    attachmen.write(fns)
    attachmen.close
    message = MessageSchema(
        subject=subject,
        recipients=[email_to],
        body=html,
        subtype=MessageType.html,
        attachments=[

            {
                "file": "/Users/rayner/Downloads/mongo_neo/neo_mongo/neo_mongo/fast_servicio_tesis/"+title_docuentss+".zip",
                "mime_type": "application",
                "mime_subtype": "zip",
            }
        ],)
    fm = FastMail(conf)

    await fm.send_message(message)
    print("mensaje enviado", fm)

    return JSONResponse(status_code=200, content={"message": "email has been sent"})
