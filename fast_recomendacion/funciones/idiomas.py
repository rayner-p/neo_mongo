from langdetect import detect


class Idiomas(object):
    def detectarIdioma(txt):
        res = detect(txt.lower())
        return res
