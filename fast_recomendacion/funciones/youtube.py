import pandas as pd
from youtube_transcript_api import YouTubeTranscriptApi
from urllib.parse import unquote

class YoutubeText(object):
    def obtenerTexto(idvideo,lbl):
        aux_texto = ''
        inicio = idvideo.find('v=')
        aux_id = idvideo[inicio+2:len(idvideo)]
        try:
            srt = YouTubeTranscriptApi.get_transcript(str(aux_id),languages=[lbl])
            for texto in srt:
                aux_texto = aux_texto + texto['text'] + ' '
            return aux_texto
        except:
            #print('texto no disponible')
            pass
    def textoYoutube(videoId):
        srt = YouTubeTranscriptApi.get_transcript(str(videoId),languages=['es'])
        return srt
    def convertirURL(url):
        inicio = url.find('https://www.youtube.com')
        fin = url.find('&sa')
        deco = url[inicio:fin]
        aux_url = unquote(deco)
        return aux_url