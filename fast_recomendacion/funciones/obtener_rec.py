# -*- coding: utf-8 -*-

from sqlalchemy import true
from config.db import conn
from funciones.webcrawler import *
from funciones.neo4jfunciones import *
from funciones.youtube import *
import pandas as pd
from concurrent.futures import ThreadPoolExecutor
from sklearn.metrics.pairwise import cosine_similarity
from stop_words import get_stop_words
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from bson.objectid import ObjectId
from graphdatascience import GraphDataScience
from langdetect import detect
from deep_translator import GoogleTranslator
from sentence_transformers import SentenceTransformer, util
from py2neo import Graph
import urllib
import urllib.request
from google_play_scraper import *
#from app_store_scraper import AppStore
from unidecode import unidecode
import time
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import tweepy
import json

from routes.user import send_email_async
# Use Neo4j URI and credentials according to your setup


class CodigoEnfermedad:
    def obtenerEnfr(code):
        gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                    name="neo4jarticulos2", auth=("neo4j", "test"))
        q_id6 = "MATCH(n:owl__Class) where '"+code + \
            "' IN n.ns1__hasDbXref return n.rdfs__label"
        resu_query = gds.run_cypher(q_id6)
        res = resu_query.to_dict('records')
        return res


class Idiomas(object):
    def detectarIdioma(txt):
        res = detect(txt.lower())
        return res


class RecursosWeb(object):

    def buscarActividadInd(aux_esp, lista_IdentificarND):
        # aux_esp = True
        # base11 = 'actividades'
        # if aux_esp:
        #     colecc11 = 'ActividadesND'
        # elif aux_esp == False:
        #     colecc11 = 'ActividadesN'
        # datsb,collection11 = Mongo.conexion(base11,colecc11)
        act_anl = []
        for i in lista_IdentificarND:
            if aux_esp:
                aux_video11 = conn.actividades.ActividadesND.find_one(
                    {"_id": ObjectId(str(i))})
            elif aux_esp == False:
                aux_video11 = conn.actividades.ActividadesN.find_one(
                    {"_id": ObjectId(str(i))})
            # aux_video11 = collection11.find_one({"_id": ObjectId(str(i))})
            act_anl.append(aux_video11['Habilidad'])
        return act_anl

    def recursosVidLibJug(aux_esp, lista_IdentificarND):
        model = SentenceTransformer('hiiamsid/sentence_similarity_spanish_es')
        # base = 'actividades'
        # coleccL = 'RecursoLibros'
        # coleccC = 'RecursoCuentos'
        # coleccJ = 'RecursoJuegos'
        # databL,collectionL = Mongo.conexion(base,coleccL)
        # datbC,collectionC = Mongo.conexion(base,coleccC)
        # datbJ,collectionJ = Mongo.conexion(base,coleccJ)
        # df_aux1 = pd.DataFrame(list(collectionL.find()))
        # df_aux2 = pd.DataFrame(list(collectionC.find()))
        # df_aux3 = pd.DataFrame(list(collectionJ.find()))

        df_aux1 = pd.DataFrame(list(conn.actividades.RecursoLibros.find()))
        df_aux2 = pd.DataFrame(list(conn.actividades.RecursoCuentos.find()))
        df_aux3 = pd.DataFrame(list(conn.actividades.RecursoJuegos.find()))

        dat1 = df_aux1['abstract'].tolist()
        dat2 = df_aux2['cuento'].tolist()
        dat3 = df_aux3['descripcion'].tolist()

        act_anl = RecursosWeb.buscarActividadInd(aux_esp, lista_IdentificarND)

        embeddings = model.encode(act_anl, convert_to_tensor=True)

        embeddingsL = model.encode(dat1, convert_to_tensor=True)
        embeddingsC = model.encode(dat2, convert_to_tensor=True)
        embeddingsJ = model.encode(dat3, convert_to_tensor=True)

        resL = util.pytorch_cos_sim(embeddings, embeddingsL)
        resC = util.pytorch_cos_sim(embeddings, embeddingsC)
        resJ = util.pytorch_cos_sim(embeddings, embeddingsJ)

        return resL, resC, resJ, df_aux1, df_aux2, df_aux3

    def guardarVideos(VideosAct):
        # aux_esp = True
        # base11 = 'actividades'
        # if aux_esp:
        #     colecc11 = 'ActividadesND'
        # elif aux_esp == False:
        #     colecc11 = 'ActividadesN'
        # colevid = 'RecursoVideos'
        # datsb,collection11 = Mongo.conexion(base11,colecc11)
        # datsb22,collection22 = Mongo.conexion(base11,colevid)
        # RecursoVideos
        # try:
        for recurso in VideosAct:
            # print(recurso['id'])
            try:
                videoG = {
                    "titulo": recurso['titulo'], 'texto': recurso['texto'], 'url': recurso['url']}
            except:
                videoG = {"titulo": recurso['titulo'], 'url': recurso['url']}
            try:
                simil = recurso['similaridad']
            except:
                simil = recurso['similaridadT']
        #     print(simil)
            # aux_video11 = collection22.find_one({"titulo": recurso['titulo']})
            aux_video11 = conn.actividades.RecursosVideos.find_one(
                {"titulo": recurso['titulo']})

            if aux_video11 is None:
                # codevid = collection22.insert_one(videoG)
                codevid = conn.actividades.RecursosVideos.insert_one(videoG)
                codev = codevid.inserted_id
            elif len(aux_video11) > 0:
                codev = aux_video11['_id']
        #         print(codevid)
            ttt = RecursoVideo().buscarVideoRecurso(codev)
            if len(ttt) == 0:
                #         print('GuardarVideo')
                recursovideo = RecursoVideo(recurso=str(codev)).save()
            elif len(ttt) > 0:
                recursovideo = ttt[0]
            # print(codev)
            if recursovideo:
                activ = Actividad()
                aux_activ = activ.buscarAct(recurso['id'])
                # print(aux_activ)
                # print(recursovideo)
                # print(simil)
                aux_activ[0].actrec.connect(
                    recursovideo, {'coseno': round(simil, 5)})
        return True
        # except:
        #     return False

    def ObtenerVideos(aux_esp, lista_IdentificarNW):
        cc = ''
        VideosAct = []
        # aux_esp = False
        # base11 = 'actividades'
        # if aux_esp:
        #     colecc11 = 'ActividadesND'
        # elif aux_esp == False:
        #     colecc11 = 'ActividadesN'
        # datsb,collection11 = Mongo.conexion(base11,colecc11)
        for i in lista_IdentificarNW:
            lbl_idioma = ['es']
            # aux_video11 = collection11.find_one({"_id": ObjectId(str(i))})
            # print(aux_video11['Habilidad'])
            if aux_esp == True:
                aux_video11 = conn.actividades.ActividadesND.find_one(
                    {"_id": ObjectId(str(i))})
            elif aux_esp == False:
                aux_video11 = conn.actividades.ActividadesN.find_one(
                    {"_id": ObjectId(str(i))})
            enf = ''
            if aux_esp:
                codigo = aux_video11['codigo']
                enf = CodigoEnfermedad.obtenerEnfr(codigo)
                enf = GoogleTranslator(source='auto', target='es').translate(
                    enf[0]['n.rdfs__label'][0].lower())
            # print(enf)

            aux_scrap = WebCrawler.BuscarActividad(
                aux_video11['Habilidad']+' niños '+enf)
            # print(aux_scrap)
            # print("/*/*/*/*/*/")
            for scp in aux_scrap:
                calcu_simi = similaridadWeb.simiAct(
                    aux_video11['Habilidad'], scp['titulo'], lbl_idioma[0])
                aux_url = YoutubeText.convertirURL(scp['url'])
                if len(aux_url) > 2:
                    textoVideo = YoutubeText.obtenerTexto(
                        aux_url, lbl_idioma[0])
                    if textoVideo:
                        calcu_simi2 = similaridadWeb.simiAct(
                            aux_video11['Habilidad'], textoVideo, lbl_idioma[0])
                        if calcu_simi2[1] > 0.10:
                            video = {'id': i, 'titulo': scp['titulo'], 'texto': textoVideo,
                                     'url': aux_url, 'similaridadT': calcu_simi[1], 'similaridad': calcu_simi2[1]}
                            VideosAct.append(video)
                    else:
                        if calcu_simi[1] > 0.10:
                            video = {
                                'id': i, 'titulo': scp['titulo'], 'url': aux_url, 'similaridadT': calcu_simi[1]}
                            VideosAct.append(video)
        # print("--*-*-*")
        print(VideosAct)
        if VideosAct:
            res_g = RecursosWeb.guardarVideos(VideosAct)
            # print(res_g)
            return res_g
        elif len(VideosAct) == 0:
            res_g = False
            return res_g
        # return VideosAct

    def obtenerSimilaresNW(id):
        dtND = pd.DataFrame(list(conn.actividades.ActividadesND.find()))
        dtNW = pd.DataFrame(list(conn.actividades.ActividadesN.find()))
        dtAR = pd.DataFrame(
            list(conn.actividades.ActividadesCapacidades.find()))
        dtL = pd.DataFrame(list(conn.actividades.libro.find()))
        dtN = pd.DataFrame(list(conn.actividades.ActividadesNuevas.find()))
        filtro = dtN['Area'] != ''
        filtro2 = dtN['Habilidad'] != ''
        nw = dtN[filtro & filtro2]

        res_a3 = []
        res_a3.extend(dtNW['Habilidad'].tolist())
        res_a3.extend(dtND['Habilidad'].tolist())
        res_a3.extend(dtAR['Actividad'].tolist())
        res_a3.extend(dtL['actividades'].tolist())
        res_a3.extend(nw['Habilidad'].tolist())

        lista_IdentificarNW = id
        stop = get_stop_words('spanish')
        tfidf = TfidfVectorizer(stop_words=stop)
        sim_cos_tfidf = tfidf.fit_transform(res_a3)
        sim_arrNW = []
        print(lista_IdentificarNW)
        for k in range(len(lista_IdentificarNW)):

            posicion_K = dtNW['_id'].tolist().index(
                ObjectId(lista_IdentificarNW[k]))
            sim_aux = []
            # similaridad del coseno entre la pagina K y toda la matriz
            cos_sim = cosine_similarity(
                sim_cos_tfidf[posicion_K], sim_cos_tfidf)
            aux_posk = 0

            sim_aux.append(lista_IdentificarNW[k])

            for i in range(len(cos_sim[0])):
                cim = []
                if i >= 0 and i < len(dtNW):
                    if cos_sim[0][i] > 0.40 and lista_IdentificarNW[k] != str(dtNW.loc[i]['_id']):
                        cim.append('ActividadesN')
                        cim.append(dtNW.loc[i]['_id'])
                        cim.append(cos_sim[0][i])
                if i >= len(dtNW) and i < len(dtND)+len(dtNW):
                    yy = len(dtNW)
                    if cos_sim[0][i] > 0.20:
                        cim.append('ActividadesNDDD')
                        cim.append(dtND.loc[i-yy]['_id'])
                        cim.append(cos_sim[0][i])
                if i >= len(dtNW)+len(dtND) and i < len(dtAR)+len(dtNW)+len(dtND):
                    yy = len(dtNW)+len(dtND)
                    if cos_sim[0][i] > 0.30:
                        cim.append('ActividadesCapacidades')
                        cim.append(dtAR.loc[i-yy]['_id'])
                        cim.append(cos_sim[0][i])
                if i >= len(dtAR)+len(dtNW)+len(dtND) and i < len(dtL)+len(dtAR)+len(dtNW)+len(dtND):
                    yy = len(dtAR)+len(dtNW)+len(dtND)
                    if cos_sim[0][i] > 0.30:
                        cim.append('libro')
                        cim.append(dtL.loc[i-yy]['_id'])
                        cim.append(cos_sim[0][i])
                if i >= len(dtL)+len(dtAR)+len(dtNW)+len(dtND) and i < len(nw)+len(dtL)+len(dtAR)+len(dtNW)+len(dtND):
                    yy = len(dtL)+len(dtAR)+len(dtNW)+len(dtND)
                    if cos_sim[0][i] > 0.30:
                        cim.append('ActividadesNuevas')
                        cim.append(nw.loc[i-yy]['_id'])
                        cim.append(cos_sim[0][i])
                if len(cim) > 0:
                    sim_aux.append(cim)
            if len(sim_aux) > 1:
                sim_arrNW.append(sim_aux)

        hhh = len(sim_arrNW)
        return sim_arrNW

    def obtenerSimilaresND(id):
        dtND = pd.DataFrame(list(conn.actividades.ActividadesND.find()))
        dtNW = pd.DataFrame(list(conn.actividades.ActividadesN.find()))
        dtAR = pd.DataFrame(
            list(conn.actividades.ActividadesCapacidades.find()))
        dtL = pd.DataFrame(list(conn.actividades.libro.find()))
        dtN = pd.DataFrame(list(conn.actividades.ActividadesNuevas.find()))
        filtro = dtN['Area'] != ''
        filtro2 = dtN['Habilidad'] != ''
        nw = dtN[filtro & filtro2]

        res_a2 = []
        res_a2.extend(dtND['Habilidad'].tolist())
        res_a2.extend(dtNW['Habilidad'].tolist())
        res_a2.extend(dtAR['Actividad'].tolist())
        res_a2.extend(dtL['actividades'].tolist())
        res_a2.extend(nw['Habilidad'].tolist())

        lista_IdentificarND = id

        stop = get_stop_words('spanish')
        tfidf = TfidfVectorizer(stop_words=stop)
        sim_cos_tfidf = tfidf.fit_transform(res_a2)
        sim_arrND = []
        for k in range(len(lista_IdentificarND)):

            posicion_K = dtND['_id'].tolist().index(
                ObjectId(lista_IdentificarND[k]))
            sim_aux = []
            # similaridad del coseno entre la pagina K y toda la matriz
            cos_sim = cosine_similarity(
                sim_cos_tfidf[posicion_K], sim_cos_tfidf)
            aux_posk = 0

            sim_aux.append(lista_IdentificarND[k])

            for i in range(len(cos_sim[0])):
                cim = []
                if i >= 0 and i < len(dtND):
                    if cos_sim[0][i] > 0.40 and lista_IdentificarND[k] != str(dtND.loc[i]['_id']):
                        cim.append('ActividadesNDDD')
                        cim.append(dtND.loc[i]['_id'])
                        cim.append(cos_sim[0][i])
                if i >= len(dtND) and i < len(dtNW)+len(dtND):
                    yy = len(dtND)
        #             print(i-yy)
                    if cos_sim[0][i] > 0.40:
                        cim.append('ActividadesN')
                        cim.append(dtNW.loc[i-yy]['_id'])
                        cim.append(cos_sim[0][i])
                if i >= len(dtNW)+len(dtND) and i < len(dtAR)+len(dtNW)+len(dtND):
                    yy = len(dtNW)+len(dtND)
        #             print(i-yy)
                    if cos_sim[0][i] > 0.20:
                        cim.append('ActividadesCapacidades')
                        cim.append(dtAR.loc[i-yy]['_id'])
                        cim.append(cos_sim[0][i])
                if i >= len(dtAR)+len(dtNW)+len(dtND) and i < len(dtL)+len(dtAR)+len(dtNW)+len(dtND):
                    yy = len(dtAR)+len(dtNW)+len(dtND)
        #             print(i-yy)
                    if cos_sim[0][i] > 0.20:
                        cim.append('libro')
                        cim.append(dtL.loc[i-yy]['_id'])
                        cim.append(cos_sim[0][i])
                if i >= len(dtL)+len(dtAR)+len(dtNW)+len(dtND) and i < len(nw)+len(dtL)+len(dtAR)+len(dtNW)+len(dtND):
                    yy = len(dtL)+len(dtAR)+len(dtNW)+len(dtND)
        #             print(i-yy)
                    if cos_sim[0][i] > 0.20:
                        cim.append('ActividadesNuevas')
                        cim.append(nw.loc[i-yy]['_id'])
                        cim.append(cos_sim[0][i])
                if len(cim) > 0:
                    sim_aux.append(cim)
            if len(sim_aux) > 1:
                sim_arrND.append(sim_aux)
        hhh = len(sim_arrND)
        return sim_arrND

    def buscarActividades(id):
        print(id[0])
        dtND = []
        dtNW = []
        aux_esp = ''
        try:
            dtND = pd.DataFrame(
                list(conn.actividades.ActividadesND.find_one({"_id": ObjectId(str(id[0]))})))
            if len(dtND):
                aux_esp = True
                return aux_esp
        except:
            pass
        try:
            dtNW = pd.DataFrame(
                list(conn.actividades.ActividadesN.find_one({"_id": ObjectId(str(id[0]))})))
            if len(dtNW):
                aux_esp = False
                return aux_esp
        except:
            pass
        return aux_esp

    def relacionesNuevasActividades(aux_esp, sim_arrND):
        gds = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                    name="neo4jarticulos2", auth=("neo4j", "test"))
        # aux_esp = False
        # base11 = 'actividades'
        # if aux_esp:
        #     colecc11 = 'ActividadesND'
        # elif aux_esp == False:
        #     colecc11 = 'ActividadesN'
        # datsb,collection11 = Mongo.conexion(base11,colecc11)
        for act_dis in sim_arrND:
            act = act_dis[0]
            aux_des = act_dis[1:len(act_dis)]

            if aux_esp:
                # aux_video11 = collection11.find_one({"_id": ObjectId(str(act_dis[0]))})
                aux_video11 = conn.actividades.ActividadesND.find_one(
                    {"_id": ObjectId(str(act_dis[0]))})
                code = aux_video11['codigo']
            elif aux_esp == False:
                # aux_video11 = collection11.find_one({"_id": ObjectId(str(act_dis[0]))})
                aux_video11 = conn.actividades.ActividadesN.find_one(
                    {"_id": ObjectId(str(act_dis[0]))})

            # if aux_esp:
            #     code = aux_video11['codigo']

            activ = Actividad()
            aux_activ = activ.buscarAct(str(act_dis[0]))

            if len(aux_activ) == 0:
                print('crearActividad')
                new_activ = Actividad(texto=str(act_dis[0])).save()
            elif len(aux_activ) > 0:
                new_activ = aux_activ[0]

            if aux_esp:
                rescp = '''MATCH(n:Actividad),(d:owl__Class)
        where n.texto='%s' AND 
        '%s' IN d.ns1__hasDbXref AND
        not exists((n)-[]->(d))
        create (n)-[:ACTIVIDAD_ENF]->(d)
        return True''' % (str(act_dis[0]), code)
                print(rescp)
                res_q = gds.run_cypher(str(rescp))
                aux_rescypher = res_q.to_dict('records')
                # print(aux_rescypher)

            for des in aux_des:
                des_act = activ.buscarAct(str(des[1]))
                if des_act:
                    # print(des_act)
                    # print(des[2])
                    new_activ.actcos2.connect(
                        des_act[0], {'coseno': round(des[2], 5)})
                    des_act[0].actcos2.connect(
                        new_activ, {'coseno': round(des[2], 5)})
        return True

    def simNeo4j(resL, df_aux1, lista_IdentificarND, tip, limite):
        aux_des = resL
        aux_rec = df_aux1
        aux_simval = []
        for i, des in enumerate(lista_IdentificarND):
            yy = []
            lis = aux_des[i]
            yy.append(des)
            for j, sim in enumerate(lis):
                if sim.item() > limite:
                    tt = []
                    aux_recdes = aux_rec['_id'][j]
                    tt.append(aux_recdes)
                    tt.append(sim.item())
                    yy.append(tt)
            if len(yy) > 1:
                aux_simval.append(yy)
# -----------------------------------------
        # tipo = '1'

        tipo = tip
        for actdes in aux_simval:
            print(actdes[0])
            actividad = Actividad()
            des_actividad = actividad.buscarAct(actdes[0])
            act_desLJC = actdes[1:len(actdes)]
            for recLJC in act_desLJC:
                if tipo == '1':
                    rcbus = RecursoLibro()
                    res = rcbus.buscarLibroRecurso(recLJC[0])
                    if len(res) > 0:
                        aux_res = res[0]
                    elif len(res) == 0:
                        aux_res = RecursoLibro(recurso=str(recLJC[0])).save()
                elif tipo == '2':
                    rcbus = RecursoCuento()
                    res = rcbus.buscarCuentoRecurso(recLJC[0])
                    if len(res) > 0:
                        aux_res = res[0]
                    elif len(res) == 0:
                        aux_res = RecursoCuento(recurso=str(recLJC[0])).save()
                elif tipo == '3':
                    rcbus = RecursoJuego()
                    res = rcbus.buscarJuegoRecurso(recLJC[0])
                    if len(res) > 0:
                        aux_res = res[0]
                    elif len(res) == 0:
                        aux_res = RecursoJuego(recurso=str(recLJC[0])).save()

                if des_actividad:
                    if tipo == '1':
                        des_actividad[0].actLib.connect(
                            aux_res, {'coseno': round(recLJC[1], 5)})
                        rec_id = recLJC[0]
                        aux_sim = recLJC[1]
                        print(rec_id)
                        print(aux_sim)
                    elif tipo == '2':
                        des_actividad[0].actCuent.connect(
                            aux_res, {'coseno': round(recLJC[1], 5)})
                        rec_id = recLJC[0]
                        aux_sim = recLJC[1]
                        print(rec_id)
                        print(aux_sim)
                    elif tipo == '3':
                        des_actividad[0].actJueg.connect(
                            aux_res, {'coseno': round(recLJC[1], 5)})
                        rec_id = recLJC[0]
                        aux_sim = recLJC[1]
                        print(rec_id)
                        print(aux_sim)
        return True

    def buscar_google_play(pala_clav, cate):
        url = 'https://play.google.com/store/search?q=' + \
            unidecode(pala_clav.replace(" ", '%20').replace(
                ",", '%2C').lower())+'&c='+cate+'&hl=es'
        print(url)
        proxy = urllib.request.ProxyHandler(
            {"http": "http://34.125.59.111:3128"})
        opener = urllib.request.build_opener(proxy)
        urllib.request.install_opener(opener)
        html = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(html, "html.parser")
        links = soup('a')
        cont_link = 0
        c = 0
        divs = soup('div')
        cont_div = 0
        l_app = []
        for i, link in enumerate(links):
            ##l_app = []
            if link != None:
                if(['Si6A0c', 'Gy4nib'] == link.get('class')):
                    cont_link += 1
                    id_app = link.get('href').split('=')[1]
                    # print(id_app)
                    result = app(
                        id_app,
                        lang='es',  # defaults to 'en'
                        country='ec'  # defaults to 'us'
                    )
                    # print(result)
                    simi_cose = simiAct(pala_clav, result['title'])
                    # print(simi_cose)
                    if(simi_cose[-1] >= 0.225 and result['genre'] == 'Educación'):
                        c += 1
                        # print(simi_cose)
                        # print(c)
                        aux_app = {'titulo': result['title'], 'descripcion': result['description'],
                                   'url': result['url'], 'similaridad': simi_cose[-1]}
                        l_app.append(aux_app)
                        time.sleep(1)
        return l_app

    def obtener_apps(aux_esp, id_l):
        #busc_acti = RecursosWeb.buscarActividadInd(aux_esp, id_l)
        # print(busc_acti)
        Appss = []
        for a in id_l:
            # print(a)
            if aux_esp == True:
                aux_app = conn.actividades.ActividadesND.find_one(
                    {"_id": ObjectId(str(a))})
            elif aux_esp == False:
                aux_app = conn.actividades.ActividadesN.find_one(
                    {"_id": ObjectId(str(a))})
            # print(aux_app['Habilidad'])
            enf = ''
            if aux_esp:
                codigo = aux_app['codigo']
                enf = CodigoEnfermedad.obtenerEnfr(codigo)
                enf = GoogleTranslator(source='auto', target='es').translate(
                    enf[0]['n.rdfs__label'][0].lower())
            aux_obt_app = RecursosWeb.buscar_google_play(
                aux_app['Habilidad']+' niños ', 'apps')
            print(aux_obt_app)
            for ap in aux_obt_app:
                aps = {'id': a, 'titulo': ap['titulo'], 'descripcion': ap['descripcion'],
                       'url': ap['url'], 'similaridad': ap['similaridad']}
                Appss.append(aps)
        print(Appss)
        if Appss:
            res_a = RecursosWeb.guardarApp(Appss)
            return res_a
            # pass
        elif len(Appss) == 0:
            res_a = False
            return res_a

    def guardarApp(AppAct):
        for recurso in AppAct:
            appG = {'titulo': recurso['titulo'],
                    'descripcion': recurso['descripcion'], 'url': recurso['url']}
            simil = recurso['similaridad']
            # print(recurso)
            value_app = conn.actividades.RecursosAppsR.find_one(
                {"titulo": recurso['titulo']})
            if value_app is None:
                app_insert = conn.actividades.RecursosAppsR.insert_one(appG)
                app_id_insert = app_insert.inserted_id
            elif len(value_app) > 0:
                app_id_insert = value_app['_id']
            aaa = RecursoApp().buscarApp(app_id_insert)
            if(len(aaa) == 0):
                recursoapp = RecursoApp(recurso=str(app_id_insert)).save()
            elif(len(aaa) > 0):
                recursoapp = aaa[0]
            if recursoapp:
                activ = Actividad()
                aux_activ = activ.buscarAct(recurso['id'])
                aux_activ[0].actApp.connect(
                    recursoapp, {'coseno': round(simil, 5)})
        return True

    def obtener_tweets(aux_esp, id_l):
        Tweets = []
        for a in id_l:
            # print(a)
            if aux_esp == True:
                aux_app = conn.actividades.ActividadesND.find_one(
                    {"_id": ObjectId(str(a))})
            elif aux_esp == False:
                aux_app = conn.actividades.ActividadesN.find_one(
                    {"_id": ObjectId(str(a))})
            # print(aux_app['Habilidad'])
            enf = ''
            if aux_esp:
                codigo = aux_app['codigo']
                enf = CodigoEnfermedad.obtenerEnfr(codigo)
                enf = GoogleTranslator(source='auto', target='es').translate(
                    enf[0]['n.rdfs__label'][0].lower())
            aux_obt_twe = RecursosWeb.tweets(aux_app['Habilidad'])

            for tw in aux_obt_twe:
                tws = {
                    'id': a, 'cuenta': tw['cuenta'], 'tweet': tw['tweet'], 'similaridad': tw['similaridad']}
                Tweets.append(tws)
        print(Tweets)
        if Tweets:
            res_t = RecursosWeb.guardarTweet(Tweets)

    def guardarTweet(TweAct):
        for recurso in TweAct:
            tweG = {'cuenta': recurso['cuenta'], 'tweet': recurso['tweet']}
            simil = recurso['similaridad']
            value_twe = conn.actividades.RecursosTweets.find_one(
                {'tweet': recurso['tweet']})
            if value_twe is None:
                twe_insert = conn.actividades.RecursosTweets.insert_one(tweG)
                twe_id_insert = twe_insert.inserted_id
            elif(len(value_twe) > 0):
                twe_id_insert = value_twe['_id']
            ttt = RecursoTweet().buscarTweet(twe_id_insert)
            if(len(ttt) == 0):
                recursotweet = RecursoTweet(recurso=str(twe_id_insert)).save()
            elif(len(ttt) > 0):
                recursotweet = ttt[0]
            if recursotweet:
                activ = Actividad()
                aux_activ = activ.buscarAct(recurso['id'])
                aux_activ[0].actTwe.connect(
                    recursotweet, {'coseno': round(simil, 5)})
        return True

    def tweets(pala_clav):
        auth = tweepy.OAuthHandler(
            "gHZLNqOcEQOtBZJNSqpX2XLTK", "RLF1A4doriQVDJ53mfiBqqZAVEeUZLyBVNIvlkhZyihM0VWzTy")
        auth.set_access_token("1576394225738031105-Nwzyf61cJCxYLdQXBKg3x5HxL5EBl0",
                              "dURko1Lju7bl6sQndoMFcJ9rutE5ehkLFHm67yGVOOYUF")
        api = tweepy.API(auth, wait_on_rate_limit=True)
        try:
            api.verify_credentials()
            print("Authentication OK")
        except:
            print("Error during authentication")
        public_tweets = api.home_timeline()
        cuen_educ = ['edupildoras', 'elcastorcurioso', 'EducacionGuao',
                     'formacion1line', 'educapeques', 'educa_aprende']
        publicaciones = []
        '''cl = 0
        cv = 0
        tt = pd.DataFrame(list(conn.actividades.RecursosTweets.find()))'''
        for c_e in cuen_educ:
            for tweet in tweepy.Cursor(api.user_timeline, screen_name=c_e, tweet_mode="extended").items(1000):
                noticia = []
                #tt = json.dumps(tweet._json,indent=2)
                publicacion = tweet._json
                fecha_noticia = publicacion["created_at"]
                texto_noticia = publicacion["full_text"]
                simi_cose = simiAct(pala_clav, texto_noticia)
                # print(simi_cose)
                if(simi_cose[-1] > 0.095):
                    aux_twe = {'cuenta': c_e, 'tweet': texto_noticia,
                               'similaridad': simi_cose[-1]}
                    publicaciones.append(aux_twe)
        return publicaciones

    async def obtenerRecursos(id, u):
        inicio = time.time()
        #print("PASO POR AQUI")
        print(id)
        res_actb = RecursosWeb.buscarActividades(id)
        print(res_actb)
        if res_actb:
            act_sim = RecursosWeb.obtenerSimilaresND(id)
            if len(act_sim) > 0:
                res_sim = RecursosWeb.relacionesNuevasActividades(
                    res_actb, act_sim)
            apps = RecursosWeb.obtener_apps(res_actb, id)
            videos = RecursosWeb.ObtenerVideos(res_actb, id)
            tw = RecursosWeb.obtener_tweets(res_actb, id)
            # print(apps)
            # print(res_sim)
            resL, resC, resJ, df_aux1, df_aux2, df_aux3 = RecursosWeb.recursosVidLibJug(
                res_actb, id)
            fin_res1 = RecursosWeb.simNeo4j(resL, df_aux1, id, '1', 0.30)
            fin_res2 = RecursosWeb.simNeo4j(resC, df_aux2, id, '2', 0.30)
            fin_res3 = RecursosWeb.simNeo4j(resJ, df_aux3, id, '3', 0.40)

            print(apps)
            print(tw)
            print(res_sim)
            print(videos)
            print(len(resL))
            print(len(resC))
            print(len(resJ))
        else:
            act_sim = RecursosWeb.obtenerSimilaresNW(id)
            if len(act_sim) > 0:
                res_sim = RecursosWeb.relacionesNuevasActividades(
                    res_actb, act_sim)
            apps = RecursosWeb.obtener_apps(res_actb, id)
            videos = RecursosWeb.ObtenerVideos(res_actb, id)
            tw = RecursosWeb.obtener_tweets(res_actb, id)

            resL, resC, resJ, df_aux1, df_aux2, df_aux3 = RecursosWeb.recursosVidLibJug(
                res_actb, id)

            fin_res1 = RecursosWeb.simNeo4j(resL, df_aux1, id, '1', 0.35)
            fin_res2 = RecursosWeb.simNeo4j(resC, df_aux2, id, '2', 0.40)
            fin_res3 = RecursosWeb.simNeo4j(resJ, df_aux3, id, '3', 0.50)

            print(apps)
            print(tw)
            print(res_sim)
            print(videos)
            print(len(resL))
            print(len(resC))
            print(len(resJ))
        await send_email_async("Recursos recopilados de actividad nueva", u)
        fin = time.time()
        print(fin-inicio)

        return True


def simiAct(act, titu):
    act1 = similaridadWeb.process_file(act)
    titu1 = similaridadWeb.process_file(titu)
    vetorizer = CountVectorizer()
    data_corpus = [act1, titu1]
    # if idma=='es':
    stop = get_stop_words('spanish')
    # elif idma == 'en':
    #    stop = get_stop_words('english')
    tfidf = TfidfVectorizer(stop_words=stop)
    res = tfidf.fit_transform(data_corpus)
    cos = cosine_similarity(res[0:1], res).flatten()
    return cos
