import requests
from bs4 import BeautifulSoup
from funciones.similaridad import *
from funciones.idiomas import *


class WebCrawler(object):
    def ObtenerCuerpo(aux_link, link):
        s = requests.Session()
        r = s.get(str(aux_link+link))
        soup = BeautifulSoup(r.text, 'html.parser')
        return soup

    def BuscarActividad(actividad):
        busqueda = []
        url = 'https://www.google.com/search?q='+actividad+'&rlz=1C1CHBD_esEC988EC988&biw=1517&bih=730&tbm=vid&sxsrf=ALiCzsYD44cLwrkaOGKmbkWQPvUUOxl2CQ%3A1655171436507&ei=bOmnYv7JHuqfkvQPz5OniA8&ved=0ahUKEwj-r4-M6qv4AhXqj4QIHc_JCfEQ4dUDCA0&uact=5&oq=Reconocemos+lugares+y+ubicaciones+Pinta+de+color+rojo+las+manzanas+que+est%C3%A1n+arriba+del+ni%C3%B1o%3B+y+de+amarillo%2C+las+que+est%C3%A1n+abajo+del+ni%C3%B1o.&gs_lcp=Cg1nd3Mtd2l6LXZpZGVvEAMyBwgjEOoCECcyBwgjEOoCECcyBwgjEOoCECcyBwgjEOoCECcyBwgjEOoCECcyBwgjEOoCECcyBwgjEOoCECcyBwgjEOoCECcyBwgjEOoCECcyBwgjEOoCECdQpAZYpAZg_xZoAXAAeACAAQCIAQCSAQCYAQCgAQGgAQKwAQrAAQE&sclient=gws-wiz-video'
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        f = soup.find_all("div", {"class": "egMi0 kCrYT"})
        if f and len(f) > 1:
            rg = len(f)-5
        else:
            rg = len(f)
        for i in range(rg):
            text = f[i]
            doc = f"""
            <html><head><title>Buscar</title></head>
            <body>
            {text}
            </body>
            </html>
            """
            soup2 = BeautifulSoup(doc)
            titulo_bus = soup2.find('div', {"class": "BNeawe vvjwJb AP7Wnd"})
            tt = titulo_bus.string
            url = soup2.div.a['href']
            aux_res = {'titulo': tt, 'url': url}
            busqueda.append(aux_res)
        return busqueda
