import pandas as pd
import re
#from spacy.lang.es.stop_words import STOP_WORDS
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics.pairwise import cosine_similarity
from stop_words import get_stop_words
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import emoji
from funciones.neo4jfunciones import Actividad


class similaridadWeb(object):

    def similaridadCosenoACT(df_cos):
        try:
            sim_cos = df_cos['actividades'].tolist()
            stop = get_stop_words('spanish')
            tfidf = TfidfVectorizer(stop_words=stop)
            sim_cos_tfidf = tfidf.fit_transform(sim_cos)
            sim_arr = []
            for k in range(len(df_cos)):
                sim_aux = []
                # similaridad del coseno entre la pagina K y toda la matriz
                cos_sim = cosine_similarity(sim_cos_tfidf[k], sim_cos_tfidf)
                sim_aux.append(df_cos.loc[k]['pagina'])
                sim_aux.append(df_cos.loc[k]['N_Actividad'])
                for i in range(len(cos_sim[0])):
                    cim = []
                    if(cos_sim[0][i] > 0.40 and df_cos.loc[k]['pagina'] != df_cos.loc[i]['pagina']):
                        cim .append(df_cos.loc[i]['pagina'])
                        cim .append(df_cos.loc[i]['N_Actividad'])
                        cim .append(cos_sim[0][i])
                    if(len(cim) > 0):
                        sim_aux.append(cim)
                if(len(sim_aux) > 2):
                    sim_arr.append(sim_aux)
            return sim_arr
        except:
            return False

    def simiAct(act, titu, idioma):
        act1 = similaridadWeb.process_file(act)
        titu1 = similaridadWeb.process_file(titu)
        vetorizer = CountVectorizer()
        data_corpus = [act1, titu1]
        if idioma == 'es':
            stop = get_stop_words('spanish')
        elif idioma == 'en':
            stop = get_stop_words('english')

        tfidf = TfidfVectorizer(stop_words=stop)
        res = tfidf.fit_transform(data_corpus)
        cos = cosine_similarity(res[0:1], res).flatten()
        return cos

    def simActividadAPI(actividad, tt):
        model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
        sentences = [actividad, tt]
        embedding_1 = model.encode(sentences[0], convert_to_tensor=True)
        embedding_2 = model.encode(sentences[1], convert_to_tensor=True)
        res = util.pytorch_cos_sim(embedding_1, embedding_2)
        return round(res.item(), 5)

    def process_file(file_name):
        file = re.sub(r'[0-9]+', '', file_name)
        file_content = similaridadWeb.deEmojify(file)
        # All to lower case
        file_content = file_content.lower()
        # Remove punctuation and spanish stopwords
        return file_content

    def deEmojify(text):
        return emoji.replace_emoji(text, replace='')

    def similaridadActividadesLibros(base, colecc):
        try:
            actividades_A = Actividad.dataFrameActividades()
            databL, collectionL = Mongo.conexion(base, colecc)
            RecursoLibrosGuardadNeo4j = []
            model = SentenceTransformer(
                'sentence-transformers/all-MiniLM-L6-v2')
            for i in actividades_A.index:
                res_lib = collectionL.find()
                for librodes in res_lib:
                    embedding_1 = model.encode(
                        actividades_A['actividades'][i], convert_to_tensor=True)
                    embedding_2 = model.encode(
                        librodes['abstract'], convert_to_tensor=True)
                    res = util.pytorch_cos_sim(embedding_1, embedding_2)
                    if res.item() > 0.65:
                        recurso_libro = {'pagina': actividades_A['pagina'][i], 'N_Actividad': actividades_A[
                            'N_Actividad'][i], 'mongoId': librodes['_id'], 'similaridad': res.item()}
                        RecursoLibrosGuardadNeo4j.append(recurso_libro)
            return RecursoLibrosGuardadNeo4j
        except:
            return False

    def similaridadActividadesCuentos(baseC, coleccC):
        try:
            actividades_A = Actividades.dataFrameActividades()
            databC, collectionC = Mongo.conexion(baseC, coleccC)
            RecursoCuentosGuardadNeo4j = []
            model = SentenceTransformer(
                'sentence-transformers/all-MiniLM-L6-v2')
            for i in actividades_A.index:
                res_lib = collectionC.find()
                for cuentodes in res_lib:
                    embedding_1 = model.encode(
                        actividades_A['actividades'][i], convert_to_tensor=True)
                    embedding_2 = model.encode(
                        cuentodes['cuento'], convert_to_tensor=True)
                    res = util.pytorch_cos_sim(embedding_1, embedding_2)
                    if res.item() > 0.65:
                        recurso_libro = {'pagina': actividades_A['pagina'][i], 'N_Actividad': actividades_A[
                            'N_Actividad'][i], 'mongoId': cuentodes['_id'], 'similaridad': res.item()}
                        RecursoCuentosGuardadNeo4j.append(recurso_libro)
            return RecursoCuentosGuardadNeo4j
        except:
            return False

    def similaridadActividadesJuegos(baseJ, coleccJ):
        try:
            actividades_A = Actividades.dataFrameActividades()
            databJ, collectionJ = Mongo.conexion(baseJ, coleccJ)
            RecursoJuegosGuardadNeo4j = []
            model = SentenceTransformer(
                'sentence-transformers/all-MiniLM-L6-v2')
            for i in actividades_A.index:
                res_jueg = collectionJ.find()
                for juegodes in res_jueg:
                    embedding_1 = model.encode(
                        actividades_A['actividades'][i], convert_to_tensor=True)
                    embedding_2 = model.encode(
                        juegodes['descripcion'], convert_to_tensor=True)
                    res = util.pytorch_cos_sim(embedding_1, embedding_2)
                    if res.item() > 0.65:
                        recurso_juego = {'pagina': actividades_A['pagina'][i], 'N_Actividad': actividades_A[
                            'N_Actividad'][i], 'mongoId': juegodes['_id'], 'similaridad': res.item()}
                        RecursoJuegosGuardadNeo4j.append(recurso_juego)
            return RecursoJuegosGuardadNeo4j
        except:
            False
