from neomodel import StructuredNode, StringProperty, RelationshipTo, RelationshipFrom, StructuredRel, config, IntegerProperty, FloatProperty
from neomodel import db
from neomodel import (config, StructuredNode, StringProperty,
                      IntegerProperty, UniqueIdProperty, RelationshipTo)

database = "neo4jarticulos2"

neo_url = 'neo4j://neo4j:test@repositoriotesisupsvirtual.online:7687'
db.set_connection(neo_url)
db._active_transaction = db.driver.session(database=database)


class FriendRel(StructuredRel):
    encoder = IntegerProperty()


class ActividadRel(StructuredRel):
    encoder = IntegerProperty()


class RelPag(StructuredRel):
    coseno = FloatProperty()


class LibPag(StructuredRel):
    encoder = IntegerProperty()


class Libro(StructuredNode):
    titulo = StringProperty(unique_index=True)
    pagina = RelationshipTo('Pagina', 'PAGINA', model=FriendRel)


class Pagina(StructuredNode):
    pagina = IntegerProperty()
    texto = StringProperty(unique_index=True)
    pagcos = RelationshipFrom('Libro', 'PAGINAS', model=LibPag)

    def buscar(self, pag):
        qr = "MATCH(p:Pagina) WHERE p.pagina = "+str(pag)+" RETURN p"
        results, meta = db.cypher_query(qr)
        return [Pagina.inflate(row[0]) for row in results]


class RelRecurso(StructuredRel):
    coseno = FloatProperty()


class RecursoVideo(StructuredNode):
    recurso = StringProperty(unique_index=True)

    def buscarVideoRecurso(self, idVideo):
        qr = "MATCH (r:RecursoVideo) WHERE r.recurso = '" + \
            str(idVideo)+"' return r"
        results, meta = db.cypher_query(qr)
        return [RecursoVideo.inflate(row[0]) for row in results]

    def buscarPaginaActivRecursoV(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:VIDEOS]-(n:RecursoVideo) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" and n.recurso='"+pag[2]+"' RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoVideo.inflate(row[0]) for row in results]

    def buscarPaginaRecursoV(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:VIDEOS]-(n:RecursoVideo) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoVideo.inflate(row[0]) for row in results]


class RecursoLibro(StructuredNode):
    recurso = StringProperty(unique_index=True)

    def buscarLibroRecurso(self, idLibro):
        qr = "MATCH (r:RecursoLibro) WHERE r.recurso = '" + \
            str(idLibro)+"' return r"
        results, meta = db.cypher_query(qr)
        return [RecursoLibro.inflate(row[0]) for row in results]

    def buscarPaginaActivRecursoL(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:LIBROS]-(n:RecursoLibro) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" and n.recurso='"+pag[2]+"' RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoLibro.inflate(row[0]) for row in results]

    def buscarPaginaRecursoL(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:LIBROS]-(n:RecursoLibro) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoLibro.inflate(row[0]) for row in results]


class RecursoCuento(StructuredNode):
    recurso = StringProperty(unique_index=True)

    def buscarCuentoRecurso(self, idVideo):
        qr = "MATCH (r:RecursoCuento) WHERE r.recurso = '" + \
            str(idVideo)+"' return r"
        results, meta = db.cypher_query(qr)
        return [RecursoCuento.inflate(row[0]) for row in results]

    def buscarPaginaActivRecursoC(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:CUENTOS]-(n:RecursoCuento) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" and n.recurso='"+pag[2]+"' RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoCuento.inflate(row[0]) for row in results]

    def buscarPaginaRecursoC(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:CUENTOS]-(n:RecursoCuento) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoCuento.inflate(row[0]) for row in results]


class RecursoJuego(StructuredNode):
    recurso = StringProperty(unique_index=True)

    def buscarJuegoRecurso(self, idVideo):
        qr = "MATCH (r:RecursoCuento) WHERE r.recurso = '" + \
            str(idVideo)+"' return r"
        results, meta = db.cypher_query(qr)
        return [RecursoJuego.inflate(row[0]) for row in results]

    def buscarPaginaActivRecursoJ(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:JUEGOS]-(n:RecursoJuego) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" and n.recurso='"+pag[2]+"' RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoJuego.inflate(row[0]) for row in results]

    def buscarPaginaRecursoJ(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:JUEGOS]-(n:RecursoJuego) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoJuego.inflate(row[0]) for row in results]


class Area(StructuredNode):
    texto = StringProperty(unique_index=True)

    def buscar(self, pag):
        qr = "MATCH (p:Area) WHERE p.texto = '"+str(pag)+"' RETURN p"
        results, meta = db.cypher_query(qr)
        return [Area.inflate(row[0]) for row in results]


class Bloque(StructuredNode):
    texto = StringProperty(unique_index=True)

    def buscar(self, pag):
        qr = "MATCH (p:Bloque) WHERE p.texto = '"+str(pag)+"' RETURN p"
        results, meta = db.cypher_query(qr)
        return [Bloque.inflate(row[0]) for row in results]


class Competencia(StructuredNode):
    texto = StringProperty(unique_index=True)

    def buscar(self, pag):
        qr = "MATCH (p:Competencia) WHERE p.texto = '"+str(pag)+"' RETURN p"
        results, meta = db.cypher_query(qr)
        return [Competencia.inflate(row[0]) for row in results]


class IndicadorEval(StructuredNode):
    clase = IntegerProperty()
    #indpag = RelationshipFrom('Pagina', 'INDICADOR',model=ActividadRel)
    indact = RelationshipFrom('Actividad', 'INDICADOR', model=ActividadRel)
    indeval = RelationshipFrom('IndicadorEval', 'SIMILAR', model=RelPag)
    indArea = RelationshipFrom('Area', 'AREA')
    indBloq = RelationshipFrom('Bloque', 'BLOQUE')
    indCompt = RelationshipFrom('Competencia', 'COMPETENCIA')

    def buscar(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:INDICADOR]-(n:IndicadorEval) WHERE p.pagina="+str(
            pag)+"  RETURN n"
        results, meta = db.cypher_query(qr)
        return [IndicadorEval.inflate(row[0]) for row in results]


class Actividad(StructuredNode):
    actividad = IntegerProperty()
    clase = IntegerProperty()
    texto = StringProperty(unique_index=True)
    actcos = RelationshipFrom('Pagina', 'ACTIVIDAD', model=ActividadRel)
    actcos2 = RelationshipFrom('Actividad', 'SIMILARIDAD', model=RelPag)
    actInd = RelationshipFrom('IndicadorEval', 'INDICADOR', model=ActividadRel)
    actrec = RelationshipFrom('RecursoVideo', 'VIDEOS', model=RelRecurso)
    actLib = RelationshipFrom('RecursoLibro', 'LIBROS', model=RelRecurso)
    actCuent = RelationshipFrom('RecursoCuento', 'CUENTOS', model=RelRecurso)
    actJueg = RelationshipFrom('RecursoJuego', 'JUEGOS', model=RelRecurso)
    actArea = RelationshipFrom('Area', 'AREA')
    actBloq = RelationshipFrom('Bloque', 'BLOQUE')
    actCompt = RelationshipFrom('Competencia', 'COMPETENCIA')
    # GAB
    actApp = RelationshipFrom('RecursoApp', 'APPS', model=RelRecurso)
    actTwe = RelationshipFrom('RecursoTweet', 'TWEETS', model=RelRecurso)

    # MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad) WHERE p.pagina = 7 and a.actividad = 2 RETURN a
    def buscar(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad) WHERE p.pagina = "+str(
            pag)+" RETURN a"
        results, meta = db.cypher_query(qr)
        return [Actividad.inflate(row[0]) for row in results]

    def buscarActividad(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad) WHERE p.pagina = "+str(
            pag[0])+" and a.actividad = "+str(pag[1])+" RETURN a"
        results, meta = db.cypher_query(qr)
        return [Actividad.inflate(row[0]) for row in results]

    def buscarAct(self, pag):
        qr = "MATCH (a:Actividad) WHERE a.texto = '"+str(pag)+"' RETURN a"
        results, meta = db.cypher_query(qr)
        return [Actividad.inflate(row[0]) for row in results]


class RecursoApp(StructuredNode):
    recurso = StringProperty(unique_index=True)

    def buscarApp(self, idApp):
        qr = "MATCH (r:RecursoApp) WHERE r.recurso = '"+str(idApp)+"' return r"
        results, meta = db.cypher_query(qr)
        return [RecursoApp.inflate(row[0]) for row in results]

    def buscarPaginaActivRecursoA(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:APPS]-(n:RecursoApp) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" and n.recurso='"+pag[2]+"' RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoApp.inflate(row[0]) for row in results]

    def buscarPaginaRecursoA(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:APPS]-(n:RecursoApp) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoApp.inflate(row[0]) for row in results]


class RecursoTweet(StructuredNode):
    recurso = StringProperty(unique_index=True)

    def buscarTweet(self, idTwe):
        qr = "MATCH (r:RecursoTweet) WHERE r.recurso = '" + \
            str(idTwe)+"' return r"
        results, meta = db.cypher_query(qr)
        return [RecursoTweet.inflate(row[0]) for row in results]

    def buscarPaginaActivRecursoT(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:TWEETS]-(n:RecursoTweet) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" and n.recurso='"+pag[2]+"' RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoTweet.inflate(row[0]) for row in results]

    def buscarPaginaRecursoT(self, pag):
        qr = "MATCH relation=(p:Pagina)-[r:ACTIVIDAD]->(a:Actividad)<-[o:TWEETS]-(n:RecursoTweet) WHERE p.pagina ="+str(
            pag[0])+" and a.actividad="+str(pag[1])+" RETURN n"
        results, meta = db.cypher_query(qr)
        return [RecursoTweet.inflate(row[0]) for row in results]
