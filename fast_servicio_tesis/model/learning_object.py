from pydantic import BaseModel


class objetoAprendizaje (BaseModel):
    tituloText: str
    parrafoText: str
    tituloAct: str
    parrafoAct: str
    textoBiblio: str
