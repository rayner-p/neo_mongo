from pydantic import EmailStr, BaseModel
from typing import List


class EmailSchema(BaseModel):
    email: List[EmailStr]


class EmailContent(BaseModel):
    email_to: str
    subject: str
