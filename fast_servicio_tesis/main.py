from fastapi import FastAPI

from route.activity import actividad
from route.learningObject import objetoActivity
from starlette.middleware.cors import CORSMiddleware as CORSMiddleware

app = FastAPI()

app.include_router(actividad)
app.include_router(objetoActivity)

origins = [
    "*",
    "http://localhost:4201",
    "http://localhost:4200",
    "http://localhost:8000",
    "http://localhost:8001",
    "http://localhost:4201",
    "http://localhost:51235",
    "http://localhost:80",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
