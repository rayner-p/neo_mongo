from py2neo import Graph
from fastapi import APIRouter
from config.db import conn
from graphdatascience import GraphDataScience
from bson.objectid import ObjectId
import pandas as pd
import logging
from pymongo import MongoClient

# from ruta.recurso import isNaN


actividad = APIRouter()


class Mongo(object):

    def obtenerColeccion():
        base = 'prueba_mongo_neo'
        colecc = 'prueba_mongo'
        client = MongoClient("localhost:27017")
        db = client[base]
        collection = db[colecc]
        try:
            # resultadors_colecc = conn.prueba_mongo_neo.prueba_mongo.find()
            resultadors_colecc = collection.find()
            print(
                'resultadors_colecc', resultadors_colecc
            )
            resultado_bases = []
            for resultado in resultadors_colecc:
                if (len(resultado) >= 10):
                    aux = []
                    aux.append(resultado['_id'])
                    aux.append(resultado['link'])
                    aux.append(resultado['title'])
                    aux.append(resultado['abstract'])
                    aux.append(resultado['authors'])
                    aux.append(resultado['keywords'])
                    aux.append(resultado['fuente'])
                    aux.append(resultado['categoriatitulo'])
                    aux.append(resultado['categoriabstract'])
                    aux.append(resultado['key'])
                    resultado_bases.append(aux)
                elif(len(resultado) < 10):
                    print('menos de 10', len(resultado))
            return resultado_bases
        except ConnectionError as exc:
            raise RuntimeError('ERROR to open database') from exc


class recomendacionPalabra:
    def buscarPalabraMongo(palabra):
        print("palabara mongo")
        from bson.objectid import ObjectId
        base_datos = Mongo.obtenerColeccion()
        lista_datos_base = []
        recursos_obtenidos_palabra_similar = []
        # rint(base_datos)
        for i in range(len(base_datos)):
            lista_datos_base.append(base_datos[i])
        # print(lista_datos_base)
        if(lista_datos_base != []):
            # list_pln = get_stop_words(palabra)
            str_match = [s for s in lista_datos_base if palabra in s]
            ids_palabras_similares = []
            for i in range(len(str_match)):
                ids_palabras_similares.append(str_match[i][0])
            # for ids_value in range(len(ids_palabras_similares)):
            #         #print(ids_palabras_similares[ids_value])
            #         query = {"_id": ObjectId(ids_palabras_similares[ids_value])}
            #         for x in collection.find(query, {"keywords": 0, "fuente": 0, "categoriatitulo": 0, "categoriabstract": 0}):
            #             recursos_obtenidos_palabra_similar.append(x)
        ids_palabras_similares = pd.DataFrame(ids_palabras_similares)
        return ids_palabras_similares

    def regexPalabra(palabra):
        base = 'prueba_mongo_neo'
        colecc = 'prueba_mongo'
        client = MongoClient("localhost:27017")
        db = client[base]
        try:
            collection = db[colecc]
            #collection = (conn.prueba_mongo_neo.prueba_mongo.find())
        except Exception as e:
            print("Error al reggex", e)
            pass
        recursos_obtenidos_palabra_similars = []
        query = {"abstract": {"$regex": palabra}}
        for x in collection.find(query, {"link": 0, "authors": 0, "keywords": 0, "fuente": 0, "categoriatitulo": 0, "categoriabstract": 0}):
            aux = []
            aux.append(x['_id'])
            aux.append(x['title'])
            aux.append(x['abstract'])
            aux.append(x['key'])
            recursos_obtenidos_palabra_similars.append(aux)
        lista_datos_base = []
        ids_palabras_similares_regex = []
        for i in range(len(recursos_obtenidos_palabra_similars)):
            lista_datos_base.append(recursos_obtenidos_palabra_similars[i])
        if(lista_datos_base != []):
            for i in range(len(lista_datos_base)):
                ids_palabras_similares_regex.append(lista_datos_base[i][0])
        ids_palabras_similares_regex = pd.DataFrame(
            ids_palabras_similares_regex)
        return ids_palabras_similares_regex


@actividad.get('/nodeSimilarity{busqueda}')
def recomendacionNodeSimilarity(busqueda: str):
    logging.info({'entra a nodeSimi'})

    graph = Graph("neo4j://repositoriotesisupsvirtual.online:7687",
                  name="neo4jactividades", auth=("neo4j", "test"))

    # CALL gds.graph.project('myGraphy','title','similaridad',{relationshipProperties: 'peso'})
    try:
        sentencia = "CALL gds.graph.drop('prob3') YIELD graphName;"
        sentencia2 = "CALL gds.graph.drop('myGraphy') YIELD graphName;"
        graph.run(sentencia)
        graph.run(sentencia2)
    except Exception as e:
        print("Error: ", e)
        pass
    create_graph = graph.run(
        "CALL gds.graph.project('prob3',['title', 'owl__Class'],{similaridad: {properties: {strength: {property: 'peso'}}}})")
    create_graph2 = graph.run(
        "CALL gds.graph.project('myGraphy','title','similaridad',{relationshipProperties: 'peso'})")
    # -------------
    print("creado")

    def runPageR():
        try:
            page_rank = graph.run(
                "CALL gds.pageRank.stream('myGraphy') YIELD nodeId, score RETURN gds.util.asNode(nodeId).uid_title AS uid_title, score ORDER BY score DESC, uid_title ASC").to_data_frame()
        except Exception as e:
            print("Error al correr el degree", page_rank)
            pass
        return page_rank

    def runClosser():
        closers = graph.run(
            "CALL gds.beta.closeness.stream('prob3') YIELD nodeId, score RETURN gds.util.asNode(nodeId).uid_title AS id, score ORDER BY score DESC").to_data_frame()
        return closers

    def runDegree():
        try:
            degree = graph.run(
                "CALL gds.degree.stream('prob3') YIELD nodeId, score RETURN gds.util.asNode(nodeId).uid_title AS name, score AS followers ORDER BY followers DESC, name ASC").to_data_frame()
        except Exception as e:
            print("Error al correr el degree", degree)
            pass
        return degree

    def runArticle():
        article_rank = graph.run(
            "CALL gds.articleRank.stream('prob3')  YIELD nodeId, score RETURN gds.util.asNode(nodeId).uid_title AS name, score ORDER BY score DESC, name ASC").to_data_frame()
        return article_rank

    def runNodeSimi():
        try:
            node_simi_recom = graph.run("CALL gds.nodeSimilarity.stream('prob3')"+"  YIELD node1, node2, similarity " +
                                        "  RETURN  gds.util.asNode(node1).uid_title AS titulo_uid_1, gds.util.asNode(node2).uid_title AS titulo_uid_2, similarity " + " ORDER BY similarity ASC, titulo_uid_1, titulo_uid_2").to_data_frame()
        except Exception as e:
            print("Error al correr el degree", node_simi_recom)
            pass
        return node_simi_recom

    def runEigenSimi():
        eigenvector = graph.run(
            "CALL gds.eigenvector.stream('prob3') YIELD nodeId, score RETURN gds.util.asNode(nodeId).uid_title AS name, score ORDER BY score DESC, name ASC").to_data_frame()
        return eigenvector
    resultado_recomendacion_arti_normal = recomendacionPalabra.buscarPalabraMongo(
        busqueda)
    resultado_recomendacion_arti_regex = recomendacionPalabra.regexPalabra(
        busqueda)
    get_page_rank = runPageR()
    get_degree_simi = runDegree()

    def pageRank(get_page_rank, resultado_recomendacion_arti_normal, resultado_recomendacion_arti_regex):
        recomendacion_list_pagerank_normal = []
        recomendacion_list_pagerank_regex = []
        for i in get_page_rank.iloc[:, 0]:
            for j in resultado_recomendacion_arti_normal.iloc[:, 0]:
                if (ObjectId(i) == ObjectId(j)):
                    recomendacion_list_pagerank_normal.append(i)
                    print('simi')
                else:
                    pass
            for z in resultado_recomendacion_arti_regex.iloc[:, 0]:
                if (ObjectId(i) == ObjectId(z)):
                    recomendacion_list_pagerank_regex.append(i)
                    print('simi regex')
                else:
                    pass
        return recomendacion_list_pagerank_normal

    def closserRecomendation(closeness, resultado_recomendacion_arti_normal, resultado_recomendacion_arti_regex):
        recomendacion_list_closs_normal = []
        recomendacion_list_closs_regex = []
        for i2 in closeness.iloc[:, 0]:
            for j2 in resultado_recomendacion_arti_normal.iloc[:, 0]:
                if (ObjectId(i2) == ObjectId(j2)):
                    recomendacion_list_closs_normal.append(i2)
                    print('simi')
                else:
                    pass
            for z2 in resultado_recomendacion_arti_regex.iloc[:, 0]:
                if (ObjectId(i2) == ObjectId(z2)):
                    recomendacion_list_closs_regex.append(i2)
                    print('simi regex')
                else:
                    pass
        return recomendacion_list_closs_regex
    print("degre")

    def degreeRecomendation(get_degree_simi, resultado_recomendacion_arti_normal, resultado_recomendacion_arti_regex):
        print("degre 2")

        recomendacion_list_degre_normal = []
        recomendacion_list_degre_regex = []
        for i3 in get_degree_simi.iloc[:, 0]:
            for j3 in resultado_recomendacion_arti_normal.iloc[:, 0]:
                if (ObjectId(j3) == ObjectId(i3)):
                    recomendacion_list_degre_normal.append(i3)

                else:
                    pass
            for z3 in resultado_recomendacion_arti_regex.iloc[:, 0]:
                if (ObjectId(z3) == ObjectId(i3)):
                    recomendacion_list_degre_regex.append(i3)
                else:
                    pass
        recomendacion_list_degre_regex = pd.DataFrame(
            recomendacion_list_degre_regex)
        return recomendacion_list_degre_normal

    get_simi_degree = degreeRecomendation(
        get_degree_simi, resultado_recomendacion_arti_normal, resultado_recomendacion_arti_regex)

    recursos_obtenidos_palabra_similar = []
    base = 'prueba_mongo_neo'
    colecc = 'prueba_mongo'
    client = MongoClient("localhost:27017")
    db = client[base]
    collection = db[colecc]
    # db,
    # collection = list(conn.prueba_mongo_neo.prueba_mongo.find())
    print('mandar a buscar los resultados')
    if (get_simi_degree != []):
        try:

            for ids_value in range(len(get_simi_degree[0])):
                # print('entra for')
                query = {"_id": ObjectId(get_simi_degree[ids_value])}
                for x in collection.find(query, {"_id": 0, "keywords": 0, "categoriatitulo": 0, "categoriabstract": 0}):
                    recursos_obtenidos_palabra_similar.append(x)

            if len(recursos_obtenidos_palabra_similar) == 0:
                return {}
            else:
                logging.info("ENTRA AL METODO POST")
                resultado_bases = []
                for resultado in recursos_obtenidos_palabra_similar:
                    colec = {
                        "link": resultado['link'],
                        "title": resultado['title'],
                        "abstract": resultado['abstract'],
                        "authors": resultado['authors'],
                        "fuente": resultado['fuente'],
                        "key": resultado['key'],
                    }
                    resultado_bases.append(colec)
                resultado_bases = pd.DataFrame(resultado_bases)
                print('pasa antes del')
                # resultado_concatenado = pd.concat([resultado_bases], ignore_index=True)
                json = resultado_bases.to_json(orient='index')
                # print('json', json)
                return json
        except:
            raise ValueError(
                "Sorry, not found articles similaritys at all")
    else:
        return ValueError("NO HAY DATOS EN LA BUSQUEDA ")
