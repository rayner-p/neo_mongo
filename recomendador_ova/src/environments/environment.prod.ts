export const environment = {
	firebase: {
		projectId: "tesisfronted",
		appId: "1:219573706175:web:31df9ab0156d606cfa550a",
		storageBucket: "tesisfronted.appspot.com",
		locationId: "europe-west",
		apiKey: "AIzaSyAojed9zdRnZY7Nhixpti0ii7Bdyhax2rw",
		authDomain: "tesisfronted.firebaseapp.com",
		messagingSenderId: "219573706175",
		measurementId: "G-2YRYZEE2ES",
	},
	production: true,
	apiUrlNewUse: "repositoriotesisupsvirtual.online:8001/users",
	//
	apiUrlGetOneAct: "repositoriotesisupsvirtual.online:8001/actividad/",
	apiUrlGetAct: "repositoriotesisupsvirtual.online:8001/actividades",
	apiUrlGetActRecPR:
		"repositoriotesisupsvirtual.online:8001/actividadesrecomendadas",
	apiUrlSetActRecDisPR:
		"repositoriotesisupsvirtual.online:8001/actividadesrecomendadas_d",
	apiUrlGetActEsp: "repositoriotesisupsvirtual.online:8001/actividades_d",
	apiUrlSetActEsp: "repositoriotesisupsvirtual.online:8001/actividades_d",
	apiUrlGetOneActEsp: "repositoriotesisupsvirtual.online:8001/actividad_d/",
	apiUrlGetActReci: "repositoriotesisupsvirtual.online:8001/actividades_reci",
	apiUrlGetActDiscReci:
		"repositoriotesisupsvirtual.online:8001/actividades_disc_reci",
	apiUrlSetActDiscReci:
		"repositoriotesisupsvirtual.online:8001/actividades_disc_reci",
	apiUrlOneActReci: "repositoriotesisupsvirtual.online:8001/acti_reci/",
	apiUrlOneActEspeReci:
		"repositoriotesisupsvirtual.online:8001/acti_espe_reci/",
	apiUrlActLeng: "repositoriotesisupsvirtual.online:8001/acti_leng",
	apiUrlOneRes: "repositoriotesisupsvirtual.online:8001/recurso/",
	apiUrlGetOneHab: "repositoriotesisupsvirtual.online:8001/habilidad/",
	apiUrlGetHab: "repositoriotesisupsvirtual.online:8001/habilidades",
	apiUrlNewAct: "repositoriotesisupsvirtual.online:8001/habilidades",
	apiUrlGetInd: "repositoriotesisupsvirtual.online:8001/indicadores",
	apiUrlGetAre: "repositoriotesisupsvirtual.online:8001/areas",
	apiUrlGetIcd: "repositoriotesisupsvirtual.online:8001/icd10",
	apiUrlGetIcdLan: "repositoriotesisupsvirtual.online:8001/icd10lan",
	apiUrlGetBlo: "repositoriotesisupsvirtual.online:8001/bloques",
	apiUrlGetCom: "repositoriotesisupsvirtual.online:8001/competencias",
	apiUrlGetRes: "repositoriotesisupsvirtual.online:8001/obtenerrecursos",
	//
	//
	apiUrlLog: "repositoriotesisupsvirtual.online:8001/login",
};
