// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	firebase: {
		projectId: "tesisfronted",
		appId: "1:219573706175:web:31df9ab0156d606cfa550a",
		storageBucket: "tesisfronted.appspot.com",
		locationId: "europe-west",
		apiKey: "AIzaSyAojed9zdRnZY7Nhixpti0ii7Bdyhax2rw",
		authDomain: "tesisfronted.firebaseapp.com",
		messagingSenderId: "219573706175",
		measurementId: "G-2YRYZEE2ES",
		databaseURL: "https://tesisfronted-default-rtdb.firebaseio.com",
	},
	production: false,
	// apiUrlNewUse: "http://repositoriotesisupsvirtual.online:8001/users",
	// //
	// apiUrlGetOneAct: "http://repositoriotesisupsvirtual.online:8001/actividad/",
	// apiUrlGetAct: "http://repositoriotesisupsvirtual.online:8001/actividades",
	// apiUrlGetActRecPR:
	// 	"http://repositoriotesisupsvirtual.online:8001/actividadesrecomendadas",
	// apiUrlSetActRecDisPR:
	// 	"http://repositoriotesisupsvirtual.online:8001/actividadesrecomendadas_d",
	// //
	// apiUrlGetActEsp:
	// 	"http://repositoriotesisupsvirtual.online:8001/actividades_d",
	// apiUrlSetActEsp:
	// 	"http://repositoriotesisupsvirtual.online:8001/actividades_d",
	// apiUrlGetOneActEsp:
	// 	"http://repositoriotesisupsvirtual.online:8001/actividad_d/",
	// apiUrlGetActReci:
	// 	"http://repositoriotesisupsvirtual.online:8001/actividades_reci",
	// apiUrlGetActDiscReci:
	// 	"http://repositoriotesisupsvirtual.online:8001/actividades_disc_reci",
	// apiUrlSetActDiscReci:
	// 	"http://repositoriotesisupsvirtual.online:8001/actividades_disc_reci",
	// apiUrlOneActReci: "http://repositoriotesisupsvirtual.online:8001/acti_reci/",
	// apiUrlOneActEspeReci:
	// 	"http://repositoriotesisupsvirtual.online:8001/acti_espe_reci/",
	// apiUrlActLeng: "http://repositoriotesisupsvirtual.online:8001/acti_leng",
	// apiUrlOneRes: "http://repositoriotesisupsvirtual.online:8001/recurso/",
	// apiUrlGetOneHab: "http://repositoriotesisupsvirtual.online:8001/habilidad/",
	// apiUrlGetHab: "http://repositoriotesisupsvirtual.online:8001/habilidades",
	// apiUrlNewAct: "http://repositoriotesisupsvirtual.online:8001/habilidades",
	// apiUrlGetInd: "http://repositoriotesisupsvirtual.online:8001/indicadores",
	// apiUrlGetAre: "http://repositoriotesisupsvirtual.online:8001/areas",
	// apiUrlGetIcd: "http://repositoriotesisupsvirtual.online:8001/icd10",
	// apiUrlGetIcdLan: "http://repositoriotesisupsvirtual.online:8001/icd10lan",
	// apiUrlGetBlo: "http://repositoriotesisupsvirtual.online:8001/bloques",
	// apiUrlGetCom: "http://repositoriotesisupsvirtual.online:8001/competencias",
	// apiUrlGetRes: "http://repositoriotesisupsvirtual.online:8001/obtenerrecursos",
	// //
	// //
	// apiUrlLog: "http://repositoriotesisupsvirtual.online:8001/login",
	apiUrlNewUse: "http://127.0.0.1:8000/users",
	//
	apiUrlGetOneAct: "http://127.0.0.1:8000/actividad/",
	apiUrlGetAct: "http://127.0.0.1:8000/actividades",
	apiUrlGetActRecPR: "http://127.0.0.1:8000/actividadesrecomendadas",
	apiUrlSetActRecDisPR: "http://127.0.0.1:8000/actividadesrecomendadas_d",
	//
	apiUrlGetActEsp: "http://127.0.0.1:8000/actividades_d",
	apiUrlSetActEsp: "http://127.0.0.1:8000/actividades_d",
	apiUrlGetOneActEsp: "http://127.0.0.1:8000/actividad_d/",
	apiUrlGetActReci: "http://127.0.0.1:8000/actividades_reci",
	apiUrlGetActDiscReci: "http://127.0.0.1:8000/actividades_disc_reci",
	apiUrlSetActDiscReci: "http://127.0.0.1:8000/actividades_disc_reci",
	apiUrlOneActReci: "http://127.0.0.1:8000/acti_reci/",
	apiUrlOneActEspeReci: "http://127.0.0.1:8000/acti_espe_reci/",
	apiUrlActLeng: "http://127.0.0.1:8000/acti_leng",
	apiUrlOneRes: "http://127.0.0.1:8000/recurso/",
	apiUrlGetOneHab: "http://127.0.0.1:8000/habilidad/",
	apiUrlGetHab: "http://127.0.0.1:8000/habilidades",
	apiUrlNewAct: "http://127.0.0.1:8000/habilidades",
	apiUrlGetInd: "http://127.0.0.1:8000/indicadores",
	apiUrlGetAre: "http://127.0.0.1:8000/areas",
	apiUrlGetIcd: "http://127.0.0.1:8000/icd10",
	apiUrlGetIcdLan: "http://127.0.0.1:8000/icd10lan",
	apiUrlGetBlo: "http://127.0.0.1:8000/bloques",
	apiUrlGetCom: "http://127.0.0.1:8000/competencias",
	apiUrlGetRes: "http://127.0.0.1:8000/obtenerrecursos",
	apiUrlGetBack: "http://127.0.0.1:8000/objetoAprendizaje",
	//
	//
	apiUrlLog: "http://127.0.0.1:8000/logins",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
