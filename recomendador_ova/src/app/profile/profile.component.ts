import { Component, OnInit } from "@angular/core";
import { User } from "firebase/auth";
import { AuthService } from "../services/auth.service";

@Component({
	selector: "app-profile",
	templateUrl: "./profile.component.html",
	styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnInit {
	constructor(public auth: AuthService) {}

	ngOnInit() {}

	logout() {
		this.auth.SignOut();
	}
}
