export class User {
	uid: string;
	password: string;
	username: string;
	email: string;
	displayName: string;
	photoURL: string;
	emailVerified: boolean;
	constructor(auth) {
		this.uid = auth.uid;
	}
}
