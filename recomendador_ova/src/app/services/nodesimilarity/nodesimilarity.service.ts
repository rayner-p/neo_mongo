import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, Output } from "@angular/core";

import { Observable } from "rxjs/internal/Observable";
import { environment } from "src/environments/environment";
const httpOptionsJWT = {
	headers: new HttpHeaders({ "Content-type": "application/json" }),
};

@Injectable({
	providedIn: "root",
})
export class NodesimilarityService {
	constructor(private usrHttp: HttpClient) {}

	public get(url: string) {
		console.log("realizando un get");
		return this.usrHttp.get(url);
	}

	public post(body: any) {
		console.log("llego al servicio", "con", { body });
		return this.usrHttp.get<any>(
			"http://127.0.0.1:8000/nodeSimilarity" + body,
			httpOptionsJWT
		);
	}
	// get_act_dis(ids: any): Observable<any> {
	// 	return this.usrHttp.get(
	// 		"http://127.0.0.1:8000/nodeSimilarity" + { busqueda: ids },
	// 		httpOptionsJWT
	// 	);
	// }
	get_indicadores() {
		//return this.usrHttp.get("http://127.0.0.1:8000/indicadores");
		return this.usrHttp.get(environment.apiUrlGetInd, httpOptionsJWT);
	}
}
