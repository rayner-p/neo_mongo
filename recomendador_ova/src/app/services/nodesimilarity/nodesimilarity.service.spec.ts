import { TestBed } from '@angular/core/testing';

import { NodesimilarityService } from './nodesimilarity.service';

describe('NodesimilarityService', () => {
  let service: NodesimilarityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NodesimilarityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
