import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root",
})
export class BetweensimilarityService {
	constructor(private http: HttpClient) {}
	public get(url: string) {
		console.log("realiza un get");
		return this.http.get(url);
	}

	public post(url: string, body: any) {
		console.log("llego al servicio", url, "con", body);
		return this.http.post<any>(url, { busqueda: body });
	}
}
