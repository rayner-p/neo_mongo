import { TestBed } from '@angular/core/testing';

import { BetweensimilarityService } from './betweensimilarity.service';

describe('BetweensimilarityService', () => {
  let service: BetweensimilarityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BetweensimilarityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
