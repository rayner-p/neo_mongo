import { TestBed } from '@angular/core/testing';

import { RestRelationSimilarityService } from './rest-relation-similarity.service';

describe('RestRelationSimilarityService', () => {
  let service: RestRelationSimilarityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestRelationSimilarityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
