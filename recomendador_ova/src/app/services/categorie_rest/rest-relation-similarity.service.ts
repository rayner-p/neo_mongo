import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
	providedIn: "root",
})
export class RestRelationSimilarityService {
	constructor(private http: HttpClient) {}
	public post(url: string, body: any) {
		console.log("llego al servicio", url, "con", body);
		return this.http.post<any>(url, { busqueda: body });
	}
}
