import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Injectable } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
const httpOptionsJWT = {
	headers: new HttpHeaders({ "Content-type": "application/json" }),
};
@Injectable({
	providedIn: "root",
})
export class OvaService {
	constructor(private usrHttp: HttpClient) {}

	public get(url: string) {
		console.log("realizando un get");
		return this.usrHttp.get(url);
	}

	public post(objetoAprndizaje: any, user: any) {
		console.log("llega al ova", objetoAprndizaje, user);
		return this.usrHttp.post<any>(
			"http://127.0.0.1:8000/objetosAprendizajes" + objetoAprndizaje,
			user + { headers: httpOptionsJWT }
		);
	}
}
