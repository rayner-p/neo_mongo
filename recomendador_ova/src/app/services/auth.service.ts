import { Injectable, NgZone } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/compat/auth";
import {
	AngularFirestore,
	AngularFirestoreDocument,
} from "@angular/fire/compat/firestore";
import { Router } from "@angular/router";
import { error } from "console";
import firebase from "firebase/compat/app";
import { of, switchMap } from "rxjs";
import { Observable } from "rxjs/internal/Observable";
import { User } from "./user";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	userData: any;
	user: Observable<User>;

	constructor(
		public router: Router,
		public auth: AngularFireAuth,
		private bd: AngularFirestore,
		public ngZone: NgZone
	) {
		this.auth.authState.subscribe((user) => {
			if (user) {
				this.userData = user;
				localStorage.setItem("user", JSON.stringify(this.userData));
				JSON.parse(localStorage.getItem("user")!);
				return this.bd.doc<User>(`users/${user.uid}`).valueChanges();
			} else {
				localStorage.setItem("user", "null");
				JSON.parse(localStorage.getItem("user")!);
			}
		});
		// this.auth.authState.subscribe((user) => {
		// 	if (user) {
		// 		//localStorage.setItem("user", JSON.stringify(this.userData));
		// 		this.userData = user;
		// 		localStorage.setItem("user", JSON.stringify(this.userData));
		// 		JSON.parse(localStorage.getItem("user"));
		// 		console.log("user json", this.userData);
		// 		return this.bd.doc<User>(`users/${user.uid}`).valueChanges();
		// 	} else {
		// 		// localStorage.setItem("login", "null");
		// 		// let json_ob_f = JSON.parse(localStorage.getItem("user")!);
		// 		localStorage.setItem("user", null);
		// 		let json_ob_f = JSON.parse(localStorage.getItem("user"));
		// 		console.log("no log", json_ob_f);
		// 	}
		// });

		// this.user = this.auth.authState.pipe(
		// 	switchMap((user) => {
		// 		if (user) {
		// 			this.userData = user;
		// 			localStorage.setItem("user", JSON.stringify(this.userData));
		// 			JSON.parse(localStorage.getItem("user"));
		// 			return this.bd.doc<User>(`users/${user.uid}`).valueChanges();
		// 		} else {
		// 			localStorage.setItem("login", "null");
		// 			let json_ob_f = JSON.parse(localStorage.getItem("user")!);
		// 			//localStorage.setItem("user", null);
		// 			//JSON.parse(localStorage.getItem("user"));
		// 			//return of(null);
		// 		}
		// 	})
		// );
	}
	// // Login with email/password
	// SignIn(email: string, password: string) {
	// 	return new Promise(async (resolve, reject) => {
	// 		this.auth
	// 			.signInWithEmailAndPassword(email, password)
	// 			.then((result) => {
	// 				console.log("resukta login ", result);
	// 				// this.updateUserData("", result.user);
	// 				this.auth.authState.subscribe((user) => {
	// 					if (user) {
	// 						console.log("navegnado");
	// 						//this.router.navigate(["/profile"]);
	// 						this.updateUserData("", result.user);
	// 					}
	// 				});
	// 			})
	// 			.catch((error) => {
	// 				window.alert(error.message);
	// 			});
	// 	});
	// }
	SignIn(email: string, password: string) {
		return this.auth
			.signInWithEmailAndPassword(email, password)
			.then((result) => {
				this.updateUserData(result.user);
				this.auth.authState.subscribe((user) => {
					if (user) {
						this.router.navigate(["/profile"]);
						console.log("navega");
					}
				});
			})
			.catch((error) => {
				window.alert(error.message);
			});
	}
	// Register  with user/email/password
	async SignUp(name, email: string, password: string) {
		const responseOk = await this.auth.createUserWithEmailAndPassword(
			email,
			password
		);
		// .then(async (result) => {
		// 	console.log("ANTES DE GUARDAR DATOS", result.user);
		// 	if ((result.user.displayName = null)) {
		// 		await responseOk.user.updateProfile({
		// 			displayName: name,
		// 		});
		// 		let userDoc = this.bd.collection("users").doc(responseOk.user.uid);

		// 		await userDoc.set({
		// 			amountInAccount: 0,
		// 			userName: responseOk.user.displayName,
		// 		});
		// 		console.log("userDoc setted");
		// 		return responseOk;
		// 	}
		console.log(responseOk.user);
		if (responseOk.user.displayName == null) {
			await responseOk.user.updateProfile({
				displayName: name,
			});
			let userDoc = this.bd.collection("users").doc(responseOk.user.uid);

			await userDoc.set({
				amountInAccount: 0,
				userName: responseOk.user.displayName,
			});
			console.log("update", responseOk.user);
		}

		this.SendVerificationMail();
		this.updateUserData(responseOk.user);

		return responseOk;
	}

	// Send email verfificaiton when new user sign up
	SendVerificationMail() {
		return this.auth.currentUser
			.then((u: any) => u.sendEmailVerification())
			.then(() => {
				this.router.navigate(["/login"]);
			});
	}
	// Reset Forggot password
	ForgotPassword(passwordResetEmail: string) {
		return this.auth
			.sendPasswordResetEmail(passwordResetEmail)
			.then(() => {
				window.alert("Password reset email sent, check your inbox.");
			})
			.catch((error) => {
				window.alert(error);
			});
	}
	// Returns true when user is looged in and email is verified
	get isLoggedInEmail(): boolean {
		const user = JSON.parse(localStorage.getItem("user")!);
		console.log("user obtenido local", user);
		//&& user.emailVerified == false ? true : false
		return user != null ? true : false;
	}
	// get isLoggedIn() {
	// 	const user = JSON.parse(localStorage.getItem("user")!);
	// 	if (localStorage.getItem("login") == "true") return true;
	// 	return false;
	// }

	login3() {
		return new Promise(async (resolve, reject) => {
			this.auth
				.signInWithPopup(new firebase.auth.GoogleAuthProvider())
				.then((credential) => {
					this.updateUserData(credential.user);
					resolve(true);
				})
				.catch((error) => {
					resolve(false);
					console.log(error);
				});
		});
	}
	// Sign out
	SignOut() {
		return this.auth.signOut().then(() => {
			localStorage.removeItem("user");
			console.log("cerrado sesison", this.user);
			this.router.navigate(["login"]);
		});
	}
	searchArticle() {
		window.alert("funciona");
	}

	async updateUserData(user: any) {
		const userRef: AngularFirestoreDocument<User> = this.bd.doc(
			`users/${user.uid}`
		);
		const userDatas: typeof user = {
			uid: user.uid,
			email: user.email,
			displayName: user.displayName,
			photoURL: user.photoURL,
			emailVerified: user.emailVerified,
		};
		if (userDatas.displayName == null) {
		} else {
			return await userRef.set(userDatas, {
				merge: true,
			});
		}
	}
	validateUserRegister() {
		const userObtenido = JSON.parse(localStorage.getItem("user")!);
	}
	esEmailValido(email: string): boolean {
		let mailValido = false;
		("use strict");

		var EMAIL_REGEX =
			/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

		if (email.match(EMAIL_REGEX)) {
			mailValido = true;
		}
		return mailValido;
	}

	/* Setting up user data when sign in with username/password,
            sign up with username/password and sign in with social auth
            provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
	SetUserDataLogin(user: any) {
		const userRef: AngularFirestoreDocument<any> = this.bd.doc(
			`users/${user.uid}`
		);
		const userDataLogin: User = {
			uid: user.uid,
			email: user.email,
			displayName: user.displayName,
			photoURL: user.photoURL,
			emailVerified: user.emailVerified,
			password: "",
			username: "",
		};
		return userRef.set(userDataLogin, {
			merge: true,
		});
	}
}
