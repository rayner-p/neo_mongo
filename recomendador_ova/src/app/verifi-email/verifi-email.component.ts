import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-verifi-email',
  templateUrl: './verifi-email.component.html',
  styleUrls: ['./verifi-email.component.scss'],
})
export class VerifiEmailComponent implements OnInit {
  constructor(public authService: AuthService) {}

  ngOnInit(): void {}
}
