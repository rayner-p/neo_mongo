import { Injectable } from "@angular/core";
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";

@Injectable({
	providedIn: "root",
})
export class AuthBasicGuard implements CanActivate {
	constructor(private auth: AuthService, private router: Router) {}
	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	):
		| Observable<boolean | UrlTree>
		| Promise<boolean | UrlTree>
		| boolean
		| UrlTree {
		console.log("escuchando");
		if (this.auth.isLoggedInEmail !== true) {
			console.log("no entra");
			this.router.navigate(["login"]);
			console.log("redirigido a login");
		}
		return true;
	}
}
