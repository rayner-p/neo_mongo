import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetweensimilarityComponent } from './betweensimilarity.component';

describe('BetweensimilarityComponent', () => {
  let component: BetweensimilarityComponent;
  let fixture: ComponentFixture<BetweensimilarityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BetweensimilarityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetweensimilarityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
