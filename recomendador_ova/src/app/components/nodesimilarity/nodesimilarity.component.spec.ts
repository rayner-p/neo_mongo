import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NodesimilarityComponent } from './nodesimilarity.component';

describe('NodesimilarityComponent', () => {
  let component: NodesimilarityComponent;
  let fixture: ComponentFixture<NodesimilarityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NodesimilarityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NodesimilarityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
