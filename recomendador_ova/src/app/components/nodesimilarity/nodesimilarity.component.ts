import {
	Component,
	EventEmitter,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import {
	animate,
	state,
	style,
	transition,
	trigger,
} from "@angular/animations";
import { Router } from "@angular/router";
// import { map } from "rxjs";
import { NodesimilarityService } from "src/app/services/nodesimilarity/nodesimilarity.service";
import { OvaService } from "src/app/services/objetoAprendizaje/ova.service";
import { busqueda } from "./busqueda";
import { NuevaActividad } from "src/app/models/nuevaactividad";
import { objetoAprendizaje } from "src/app/models/objetoAprendizaje";
import { user_mail } from "src/app/models/objetoAprendizaje";

import { AuthService } from "src/app/services/auth.service";
import { ServiciosService } from "src/app/services/servicios.service";
import { EmailAuthCredential } from "firebase/auth";
import { map } from "rxjs/operators";
import { type } from "os";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
	selector: "app-nodesimilarity",
	templateUrl: "./nodesimilarity.component.html",
	styleUrls: ["./nodesimilarity.component.css"],
	animations: [
		trigger("detailExpand", [
			state("collapsed", style({ height: "0px", minHeight: "0" })),
			state("expanded", style({ height: "*" })),
			transition(
				"expanded <=> collapsed",
				animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
			),
		]),
	],
})
export class NodesimilarityComponent implements OnInit {
	searchModel = new busqueda("");
	article = {};
	videos_obenidos = {};
	datos_id = {};
	rows: {};
	rows_videos: {};
	newarticle = {};

	new_ids_reci = {};
	prueba = [];
	value: "Actividad";

	columnsToDisplay = [
		"Titulo",
		"Abstract",
		"Fuente",
		"Categoria",
		"Link",
		"Autor",
	];
	columnsToDisplayWithExpand = [...this.columnsToDisplay, "expand"];

	p: number = 1;
	public loading: boolean;
	pageSize = [10, 20, 30];
	///---------------------
	nNew_acti: NuevaActividad = new NuevaActividad();
	new_objeto_aprendizaje: objetoAprendizaje = new objetoAprendizaje();
	user_mails: user_mail = new user_mail();
	sToken: string = "";
	sUsua!: string;
	rol: string = sessionStorage.getItem("dRol");
	fechNaci: any;
	fA: Date = new Date();
	fN!: Date;
	tiem!: number;
	options: string = "Actividad";
	option: string = "Actividad";
	lArea: any;
	lBloq: any;
	lBloq1: any[] = [];
	lComp: any;
	lComp1: any[] = [];
	lIdRecu: any[] = [];
	lines: any[] = []; //for headings
	linesR: any[] = []; // for rows
	page_csv!: number;
	conf: boolean = false;
	lIndi: any;
	nindi: boolean = false;
	ca_es: boolean = false;
	icd10_l: any;
	icd10_m: any;
	l_icd10_m: any;
	l_icd10_l: any;
	page_diagm: number = 1;
	page_diagl: number = 1;
	l_act_nue: any;
	codi: string;
	usermail: string;
	constructor(
		private restServiceSimilarity: NodesimilarityService,
		private restObjetoAprendizaje: OvaService,
		private router: Router,
		public auth: AuthService,

		private ser: ServiciosService
	) {
		this.loading = false;
		// this.restObjetoAprendizaje
		// 	.post(tituloTexto, parrafoTexto, tituloActividad, parrafoActividad, links)
		// 	.subscribe((res) => {
		// 		console.log(res);
		// 	});
	}

	ngOnInit(): void {
		this.nNew_acti.Habilidad = "";
		this.ca_es = false;
		this.nNew_acti.capa_espe = this.ca_es;
		console.log(this.nNew_acti);
	}

	manageAllRows(flag: boolean) {
		(row) => {
			row.expanded = flag;
		};
	}
	public sendForm() {
		console.log("Formulario enviado:", this.searchModel.busqueda);
	}
	public loadData() {
		console.log("cargando data....");
		this.restServiceSimilarity.get("http://127.0.0.1:8000/nodeSimilarity");
	}

	public sendSearch() {
		this.loading = true;
		console.log("mandamos:", this.searchModel.busqueda);
		this.restServiceSimilarity
			.post(this.searchModel.busqueda)
			.subscribe((respuesta) => {
				console.log("res", typeof respuesta);

				if (JSON.stringify(respuesta) == "{}") {
					alert("No hay datos con esa busqueda");
					console.log("vacio");
				} else {
					this.newarticle = JSON.stringify(respuesta);
					this.article = JSON.parse(respuesta);
					this.prueba = JSON.parse(respuesta);
					this.rows = Object.values(this.article);
					console.log("rows", this.rows);
					if (!this.newarticle) {
						alert("Error en el servidor");
					} else {
						this.loading = false;
					}
				}
			});
	}
	///-----------------
	/* Metodo para seleccionar una de las dos opciones Actividad o Habilidad */
	options_new(value: any) {
		console.log(value);
		if (value === "Actividad") {
			this.ser.get_indicadores().subscribe((i) => {
				this.lIndi = i;
				console.log("lIndi", this.lIndi);
			});
			this.lBloq1 = [];
			this.lComp1 = [];
			if (!this.ca_es) {
				this.ca_es = false;
			} else {
				this.ca_es == true;
			}
			console.log(this.ca_es);
			this.nNew_acti.capa_espe = this.ca_es;
			delete this.nNew_acti.Area;
			delete this.nNew_acti.Bloque;
			delete this.nNew_acti.Competencia;
			console.log(this.nNew_acti);
		}
	}
	/* Metodo que recibe el nombre de la Actividad/Habilidad */
	get_new_ac(value: any) {
		console.log("valor que llega al web scraping", value);
		if (value === "") {
			this.option = "";
			this.lBloq1 = [];
			this.lComp1 = [];
		} else {
			if (this.nNew_acti.indicadores === "") {
				this.nNew_acti.indicadores = "";
				this.nNew_acti.Habilidad = value;
				console.log(this.nNew_acti);
			}
			this.nNew_acti.Habilidad = value;
			console.log(this.nNew_acti);
		}
	}

	/* Metodo que recibe el indicador manualmente */
	get_new_in(value: any) {
		console.log(this.nNew_acti);
		if (value !== "") {
			this.nNew_acti.indicadores = value;
		}
	}
	/* Metodo para guardar una solo actividad/habilidad a la vez  0
: 
link
: 
"https://www.youtube.com/watch?v=JqbcU4p5qaA"
title
: 
"Exercise Testing and Prescription for Health Oriented Muscular Fitness and Flexibility"
[*/
	set_valores_ova(
		title: string,
		abstract: string,
		title_a: string,
		abstract_a: string,
		fuente: string,
		title_video: string,
		url: string
	) {
		if ((this.new_objeto_aprendizaje.url_video = ""))
			this.new_objeto_aprendizaje.url_video = this.rows_videos[0]["link"];
		if ((this.new_objeto_aprendizaje.titulo_video = ""))
			this.new_objeto_aprendizaje.titulo_video = this.rows_videos[0]["title"];

		console.log("ingresa a setear datos", title);
		this.new_objeto_aprendizaje.tituloTexto = title;
		this.new_objeto_aprendizaje.parrafoTexto = abstract;
		this.new_objeto_aprendizaje.tituloActividad = title_a;
		this.new_objeto_aprendizaje.parrafoActividad = abstract_a;
		this.new_objeto_aprendizaje.links = fuente;
		this.new_objeto_aprendizaje.titulo_video =
			this.new_objeto_aprendizaje.url_video = url;
	}
	reg_new_ah() {
		this.usermail = this.auth.userData.email;
		this.user_mails.usermail = this.usermail;
		this.l_act_nue = [];
		this.l_act_nue.push(this.nNew_acti);
		this.ser.new_acti_habi(this.l_act_nue).subscribe((re_ac_ha) => {
			this.ser
				.background_method(re_ac_ha, this.usermail)
				.subscribe((respueta_videos) => {
					this.set_valores_ova;

					this.new_ids_reci = JSON.stringify(respueta_videos);
					this.videos_obenidos = JSON.parse(Object(respueta_videos));
					this.rows_videos = Object.values(this.videos_obenidos);
					console.log("rows videos", this.rows_videos);
					this.new_objeto_aprendizaje.titulo_video =
						this.rows_videos[0]["title"];
					this.new_objeto_aprendizaje.url_video = this.rows_videos[0]["link"];

					console.log("new url ", this.rows_videos[0]["link"]);

					try {
						console.log("MANDAMOS A CREAR EL OVA");
						const new_objet = JSON.stringify(this.new_objeto_aprendizaje);
						const new_user = JSON.stringify(this.user_mails);
						this.restObjetoAprendizaje
							.post(new_objet, new_user)
							.subscribe((resz) => {
								console.log("enviado datos para el ova", resz);
							});
					} catch (e) {
						console.log("error:", e);
					}
				});

			this.nNew_acti.Habilidad = "";
			if (this.nNew_acti.indicadores != undefined) {
				delete this.nNew_acti.indicadores;
			} else if (
				this.nNew_acti.Area != undefined &&
				this.nNew_acti.Bloque != undefined &&
				this.nNew_acti.Competencia != undefined
			) {
				delete this.nNew_acti.Area;
				delete this.nNew_acti.Bloque;
				delete this.nNew_acti.Competencia;
			}

			console.log(this.nNew_acti);
			this.option = "";
			this.ca_es = false;
			this.l_icd10_m = [];
			this.l_icd10_l = [];
			console.log("recurso creado", this.lIdRecu);
		});
	}

	/* Metodo que indica si se añade un nuevo indicador manualmente */
	indi(value: true) {
		this.nindi = value;
		if (this.nindi == true) {
			this.nNew_acti.indicadores = "";
		} else {
			this.nNew_acti.indicadores = "";
		}
	}

	//---------
	public createActivity(titulo_actividad) {
		alert(
			"SU OBJETO DE APRENDIZAJE SE ESTA CREANDO. Dentro de unos minutos un correo será enviado con su archivo"
		);
		this.get_new_ac(titulo_actividad);
		this.options_new(this.value);
		this.indi(true);
		this.get_new_in("conoce las afecciones del cuerpo humano");
		this.reg_new_ah();
	}
	// public getCollections(dataentrante: any) {
	// 	console.log(dataentrante);
	// 	this.router.navigate(["relaciones", dataentrante]);
	// }

	// public makeOva(new_objeto_aprendizaje) {
	// 	console.log("ingresa", new_objeto_aprendizaje);
	// 	this.restObjetoAprendizaje
	// 		.post(this.set_valores_ova, this.user_mails)
	// 		.subscribe((resz) => {
	// 			console.log("enviado datos para el ova", resz);
	// 		});
	// }
}
