import { TestBed } from "@angular/core/testing";

import { OvaServices } from "./ova.service";

describe("OvaService", () => {
	let service: OvaServices;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(OvaServices);
	});

	it("should be created", () => {
		expect(service).toBeTruthy();
	});
});
