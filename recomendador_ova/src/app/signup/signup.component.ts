import { Component, OnInit } from "@angular/core";
import { GoogleAuthProvider } from "@angular/fire/auth";
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { AngularFirestore } from "@angular/fire/compat/firestore";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../services/auth.service";

@Component({
	selector: "app-signup",
	templateUrl: "./signup.component.html",
	styleUrls: ["./signup.component.scss"],
})
export class SignupComponent implements OnInit {
	constructor(
		//private fb: FormBuilder,
		private router: Router,
		public auth: AuthService,
		public authFs: AngularFireAuth,
		public afs: AngularFirestore
	) {}
	// signUpForm: FormGroup = this.fb.group({
	// 	email: ["", [Validators.required, Validators.email]],
	// 	password: ["", [Validators.required]],
	// 	displayName: ["", [Validators.required]],
	// 	username: ["", [Validators.required]],
	// 	uid: ["", [Validators.required]],
	// 	photoURL: ["", [Validators.required]],
	// 	emailVerified: ["", [Validators.required]],
	// });
	test: Date = new Date();
	focus;
	focus1;
	focus2;

	ngOnInit(): void {}
	// signUpViaEmail() {
	// 	this.auth.SignUp(this.signUpForm.value);
	// }

	// googleLogin() {
	// 	this.auth.GoogleAuth(this.signUpForm.value);
	// }

	GoogleAuth() {
		return this.AuthLogin(new GoogleAuthProvider());
	}
	// Auth logic to run auth providers
	AuthLogin(provider) {
		return this.authFs
			.signInWithPopup(provider)
			.then((credential) => {
				console.log("You have been successfully logged in!");
				this.updateUserData(credential.user);
			})
			.catch((error) => {
				console.log(error);
			});
	}
	SignUp(name, email, password) {
		console.log("VALORES A GUARDAR", name, email, password);
		this.auth.SignUp(name, email, password);
	}
	async login3() {
		let login = await this.auth.login3();
		if (login) {
			console.log("si se registra con google");
			this.router.navigate(["/profile"]);
		} else {
			console.log("no");
			alert("no registro");
		}
	}
	async updateUserData(user: any) {
		const usersCollection = this.afs.collection<any>("users");
		const providerData = user?.providerData[0];
		const data = {
			uid: user?.uid,
			displayName: providerData.displayName,
			email: providerData.email,
			phoneNumber: providerData.phoneNumber || "",
			providerId: providerData.providerId,
			photoURL: providerData.photoURL,
		};
		void usersCollection.doc(user.uid).set(data);
	}
}
