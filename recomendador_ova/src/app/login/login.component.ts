import { Component, Injectable, OnInit } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { AngularFirestore } from "@angular/fire/compat/firestore";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { GoogleAuthProvider } from "firebase/auth";
import firebase from "firebase/compat/app";
import { Login } from "../models/Login";
import { AuthService } from "../services/auth.service";
import { ServiciosService } from "../services/servicios.service";

@Component({
	selector: "app-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
	submitted = false;
	sToken: string = "";
	tReto: any;
	constructor(
		private router: Router,
		public auth: AngularFireAuth,
		public authSer: AuthService,
		private ser: ServiciosService,
		public afs: AngularFirestore
	) {}
	focus;
	focus1;

	ngOnInit(): void {
		this.onSubmit();
		console.log("EJECUTANDO...");
	}

	async SignIn(email, password) {
		this.authSer.SignIn(email, password);
	}
	async login3() {
		let login = await this.authSer.login3();
		if (login) {
			console.log("si ingresa");
			this.router.navigate(["/profile"]);
		} else {
			console.log("no");
			alert("no login");
		}
	}
	logout() {
		this.authSer.SignOut();
	}

	onSubmit(): void {
		this.submitted = true;
		var dataUsua = new Login();
		dataUsua.username = "tsteve@outlook.es";
		dataUsua.password = "0105662068";
		this.ser.logInJWT(dataUsua).subscribe((resu) => {});
	}
}
