import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { ProfileComponent } from "./profile/profile.component";
import { SignupComponent } from "./signup/signup.component";
import { LandingComponent } from "./landing/landing.component";
import { LoginComponent } from "./login/login.component";
import { ForgotPasswordComponent } from "./components/forgot-password/forgot-password.component";
import { VerifiEmailComponent } from "./verifi-email/verifi-email.component";
import { AuthBasicGuard } from "../app/guards/auth-basic.guard";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { NodesimilarityComponent } from "./components/nodesimilarity/nodesimilarity.component";

const routes: Routes = [
	{ path: "home", component: HomeComponent, canActivate: [AuthBasicGuard] },
	{
		path: "profile",
		component: ProfileComponent,
		canActivate: [AuthBasicGuard],
	},
	{ path: "register", component: SignupComponent },
	{
		path: "landing",
		component: LandingComponent,
	},
	{ path: "login", component: LoginComponent },

	{ path: "password", component: ForgotPasswordComponent },
	{ path: "verify-email-address", component: VerifiEmailComponent },
	{ path: "nodesimilarity", component: NodesimilarityComponent },
	{ path: "", redirectTo: "login", pathMatch: "full" },
];

@NgModule({
	imports: [
		CommonModule,
		BrowserModule,
		RouterModule.forRoot(routes, {
			useHash: true,
		}),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
