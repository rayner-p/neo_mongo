import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule } from "@angular/router";
import { AppRoutingModule } from "./app.routing";

import { AppComponent } from "./app.component";
import { SignupComponent } from "./signup/signup.component";
import { LandingComponent } from "./landing/landing.component";
import { ProfileComponent } from "./profile/profile.component";
import { HomeComponent } from "./home/home.component";
import { NavbarComponent } from "./shared/navbar/navbar.component";
import { FooterComponent } from "./shared/footer/footer.component";

import { LoginComponent } from "./login/login.component";
import { initializeApp, provideFirebaseApp } from "@angular/fire/app";
import { environment } from "../environments/environment";
import { provideAuth, getAuth } from "@angular/fire/auth";
import { provideFirestore, getFirestore } from "@angular/fire/firestore";
import { AngularFireAuthModule } from "@angular/fire/compat/auth";
import { AngularFireModule, FIREBASE_OPTIONS } from "@angular/fire/compat";
import { AuthService } from "./services/auth.service";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { CommonModule } from "@angular/common";
import { ForgotPasswordComponent } from "./components/forgot-password/forgot-password.component";
import { NodesimilarityComponent } from "./components/nodesimilarity/nodesimilarity.component";
import { HttpClientModule } from "@angular/common/http";
import { AngularFirestore } from "@angular/fire/compat/firestore";
import { NgxPaginationModule } from "ngx-pagination";
import { HomeModule } from "./home/home.module";
import { MatTableModule } from "@angular/material/table";
import { MatNativeDateModule } from "@angular/material/core";

@NgModule({
	declarations: [
		AppComponent,
		SignupComponent,
		LandingComponent,
		ProfileComponent,
		NavbarComponent,
		FooterComponent,
		LoginComponent,
		DashboardComponent,
		ForgotPasswordComponent,
		NodesimilarityComponent,
	],
	imports: [
		MatTableModule,
		NgxPaginationModule,
		CommonModule,
		BrowserModule,
		NgbModule,
		MatNativeDateModule,
		FormsModule,
		RouterModule,
		AppRoutingModule,
		HttpClientModule,
		HomeModule,
		provideFirebaseApp(() => initializeApp(environment.firebase)),
		provideAuth(() => getAuth()),
		provideFirestore(() => getFirestore()),
	],
	providers: [
		{ provide: FIREBASE_OPTIONS, useValue: environment.firebase },
		AngularFirestore,
		AuthService,
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
